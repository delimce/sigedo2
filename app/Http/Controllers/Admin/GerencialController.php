<?php
/**
 * Created by PhpStorm.
 * User: delimce
 * Date: 13/12/2015
 * Time: 10:51 PM
 */

namespace App\Http\Controllers\admin;

use App\Libs\Docs\Resumen;
use App\Models\Gerencial;
use App\Models\GerencialSubObra;
use App\Http\Controllers\Admin\ReportController;
use DB;
use Illuminate\Support\Facades\Input;
use Laravel\Lumen\Routing\Controller as BaseController;
use Log;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use Validator;

class GerencialController extends BaseController
{

    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('required:4'); ///id del modulo requerido


    }


    public function showEst()
    {

        $actual = Session::get('OBRASELECT');
        $obras = GerencialSubObra::leftJoin('tbl_gerencial_est', function ($join) use ($actual) {
            $join->on('tbl_gerencial_subobra.CodSubObr', '=', 'tbl_gerencial_est.CodSubObr')
                ->where('tbl_gerencial_est.CodObr', '=', "$actual");
        })->where("tbl_gerencial_subobra.CodObr", "=", $actual)->select("DesSubObr", "EstDocs", "tbl_gerencial_subobra.CodSubObr")->get();

        return view("admin.gerencial.est", ['data' => $obras]);

    }


    public function showReport()
    {


        if (ReportController::isObraSelected())
            $results = DB::select('EXEC [dbo].[sp_gerencial] @codObr = ?', array(Session::get('OBRASELECT')));
        else
            $results = DB::select('EXEC [dbo].[sp_gerencial_uni] ');

        $resumen = new Resumen(count($results));

        return view("admin.gerencial.report", ['data' => $results, 'resumen' => $resumen]);
    }


    public function gerencial2Excel()
    {


        if (ReportController::isObraSelected())
            $results = DB::select('EXEC [dbo].[sp_gerencial] @codObr = ?', array(Session::get('OBRASELECT')));
        else
            $results = DB::select('EXEC [dbo].[sp_gerencial_uni] ');

        $resumen = new Resumen(count($results));

        Excel::create('Reporte gerencial', function ($excel) use ($results, $resumen) {

            $excel->sheet('documentos', function ($sheet) use ($results, $resumen) {

                $sheet->loadView('admin.excel.gerencial', array('data' => $results, "resumen" => $resumen));

            });

        })->download('xlsx');
    }


    public function saveDocsEst()
    {

        $actual = Session::get('OBRASELECT');

        try {
            DB::beginTransaction();
            ////eliminando los estimados
            $estimados = Gerencial::where("CodObr", $actual);
            $estimados->delete();

            ///consultando obras
            $obras = GerencialSubObra::where("CodObr", $actual)->get();


            foreach ($obras as $obra) {

                $module = new Gerencial();
                $module->CodObr = $obra->CodObr;
                $module->CodSubObr = $obra->CodSubObr;
                $estimado = Input::get('est_' . $obra->CodSubObr); ///el estimado
                $module->EstDocs = ($estimado == "") ? 0 : $estimado;
                $module->save();

            }

            DB::commit();

        } catch (Exception $e) {
            // Woopsy
            DB::rollback();
        }


    }


}