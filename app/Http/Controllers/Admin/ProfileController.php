<?php
/**
 * Created by PhpStorm.
 * User: delimce
 * Date: 13/12/2015
 * Time: 10:51 PM
 */

namespace App\Http\Controllers\admin;

use App\Models\Module;
use App\Models\UserProfile;
use App\Models\UserProfileModule;
use DB;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Validator;
use Log;

class ProfileController extends BaseController
{

    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('required:2'); ///id del modulo requerido


    }


    public function getList()
    {
        $data = UserProfile::all();

        foreach ($data as $perfil) {
            $modulos = $perfil->module()->get();
            $modules = array();
            foreach ($modulos as $modulo) {
                $modules[] = $modulo->nombre;
            }

            $perfil["modulos"] = implode(", ", $modules); ///nombre de los modulos
        }

        $modules = Module::orderBy("id")->get();

        return view("admin.user.profileList", ['data' => $data, 'modulos' => $modules]);

    }


    public function saveOrUpdate(Request $req)
    {

        //////////validation
        $validator = Validator::make($req->all(), [
            'nombre' => 'required',
            'modulos' => 'required',

        ]);

        if ($validator->fails()) {
            return 'error';

        } else {

            try {

                DB::beginTransaction();

                // database queries here
                $model = new UserProfile();

                //////////condicion para editar
                if ($req->has('id')) {
                    $model = $model->find($req->id);
                }
                $model->descripcion = $req->nombre;
                $model->save(); ////guardando datos del perfil


                ///eliminando modulos antiguos
                $model->moduleSelected()->delete();
                ///creando nuevos modulos
                $modules = array();
                foreach ($req->modulos as $mod) {
                    $module = new UserProfileModule();
                    $module->modulo_id = $mod;
                    $modules[] = $module;
                }

                $model->moduleSelected()->saveMany($modules);

                DB::commit();

            } catch (Exception $e) {
                // Woopsy
                DB::rollback();
            }
        }


    }


    public function delete(Request $req)
    {
        try {

            $model = new UserProfile();
            $id = $req->input("id", 0);
            $model->destroy($id);
            $result = array("tipo" => "ok");

        } catch (\Illuminate\Database\QueryException $e) {

            $errorCode = $e->errorInfo[1];
            if ($errorCode == 547) {
                // houston, we have a duplicate entry problem
                $result = array("titulo" => "Error eliminando el perfil",
                    "mensaje" => "error el registro se encuentra en uso",
                    "tipo" => "error");
                Log::error($e->errorInfo[2]);

            }
        }

        return response()->json($result); /// json
    }


    public function getDataById(Request $req)
    {

        $datos = UserProfile::find($req->id);

        $modulos = $datos->moduleSelected()->get();

        foreach ($modulos as $mods) {
            $mod[] = $mods->modulo_id;
        }
        $datos["modulos"] = implode(",", $mod);

        return $datos;

    }


}