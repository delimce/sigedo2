<?php
/**
 * Created by PhpStorm.
 * User: delimce
 * Date: 5/11/2015
 * Time: 9:07 AM
 */
namespace App\Http\Controllers\Admin;

use App\Libs\Docs\Resumen;
use App\Models\Area;
use App\Models\Documento;
use App\Models\DocumentoUnico;
use App\Models\Estatus;
use App\Models\FaseProy;
use App\Models\Movimiento;
use App\Models\Obra;
use App\Models\Persona;
use App\Models\SubArea;
use App\Models\SubObra;
use App\Models\TipoDocumento;
use App\Models\Tramo;
use App\Models\UnidConstr;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Laravel\Lumen\Routing\Controller as BaseController;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use Validator;

class ReportController extends BaseController
{


    private $filtersResult = array();
    private $docs2Excel;
    private $docId; ////id del documento para el reporte

    /**
     * Create a new authentication controller instance.
     *
     * @param  Authenticator $auth
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('required:3'); ///id del modulo requerido

    }


    public function showGeneral(Request $req)
    {

        $data = $this->setFIlters();

        if ($req->has('_token')) { ///metodo post se hizo la busqueda

            $data = $this->getFilters($req, $data); ///valores de los filtros


            ///////para documentos x obra  o total
            if ($this->isObraSelected()) {
                $document = new Documento();
                $documentos = $this->filtersApply($document); ///aplicando filtros
            } else {
                $document = new DocumentoUnico();
                $documentos = $this->filtersApply2($document); ///aplicando filtros
            }
            //////////


            ////aplicando todos los filtros
            $data["docs"] = $documentos->get();

            $resumen = new Resumen($documentos->count());
            $data["resumen"] = $resumen; //objeto para resumenes


        }

        return view("admin.reports.general", $data);

    }

    /**trae los movimientos
     * @param $codDoc
     */
    public function getMovs($codDoc)
    {

        $doc = Documento::findOrFail($codDoc);
        $movs = $doc->movimiento()->get();

        $movs1 = $movs; ///temp
        $personas = array();

        foreach ($movs1 as $mov) {

            try {
                $personas[$mov->CodMov] = $mov->persona->NomEnt;
            } catch (\ErrorException $e) {
                $personas[$mov->CodMov] = "";
                log::info("no existe persona asociada para el movimiento: $mov->CodMov");
            }

        }


        return view("admin.reports.movs", ["doc" => $doc, "movs" => $movs, "personas" => $personas]);

    }


    public function showTotalxObra(Request $req)
    {

        $this->getFilters2($req); ///valores de los filtros


        $fecha1 = isset($this->filtersResult["fecha1"]) ? $this->filtersResult["fecha1"] : NULL;
        $fecha2 = isset($this->filtersResult["fecha2"]) ? $this->filtersResult["fecha2"] : NULL;

        $resumen = new Resumen(0);


        if ($req->has('_token')) {
            if ($this->isObraSelected())

                $results = DB::select('EXEC [dbo].[sp_totalxobra] @codObr = ?, @fecha1 = ?, @fecha2 = ?', array(Session::get('OBRASELECT'), $fecha1, $fecha2));

            else

                $results = DB::select('EXEC [dbo].[sp_totalxobra_uni] @fecha1 = ?, @fecha2 = ?', array($fecha1, $fecha2));

        } else
            $results = [];


        return view("admin.reports.totalObra", ["data" => $results, "fecha1" => $fecha1, "fecha2" => $fecha2, "resumen" => $resumen]);

    }


    public function showDocsxObra(Request $req)
    {

        $this->getFilters2($req); ///valores de los filtros


        $fecha1 = isset($this->filtersResult["fecha1"]) ? $this->filtersResult["fecha1"] : NULL;
        $fecha2 = isset($this->filtersResult["fecha2"]) ? $this->filtersResult["fecha2"] : NULL;


        if ($req->has('_token')) {

            if ($this->isObraSelected())
                $results = DB::select('EXEC [dbo].[sp_docsxobra] @codObr = ?, @fecha1 = ?, @fecha2 = ?', array(Session::get('OBRASELECT'), $fecha1, $fecha2));
            else
                $results = DB::select('EXEC [dbo].[sp_docsxobra_uni]  @fecha1 = ?, @fecha2 = ?', array($fecha1, $fecha2));

        } else
            $results = [];


        $resumen = New Resumen(0);
        return view("admin.reports.docsObra", ["data" => $results, "resumen" => $resumen, "fecha1" => $fecha1, "fecha2" => $fecha2]);
    }


    public function showDocsAB(Request $req)
    {

        $this->getFilters2($req); ///valores de los filtros

        $fecha1 = isset($this->filtersResult["fecha1"]) ? $this->filtersResult["fecha1"] : NULL;
        $fecha2 = isset($this->filtersResult["fecha2"]) ? $this->filtersResult["fecha2"] : NULL;

        $obra = new Obra(); ////para calcular el detalle de los documentos A y B


        if ($req->has('_token')) {
            
            if ($this->isObraSelected())
                $results = DB::select('EXEC [dbo].[sp_docsab] @codObr = ?, @fecha1 = ?, @fecha2 = ?', array(Session::get('OBRASELECT'), $fecha1, $fecha2));
            else
                $results = DB::select('EXEC [dbo].[sp_docsab_uni] @fecha1 = ?, @fecha2 = ?', array($fecha1, $fecha2));

        } else
            $results = [];


        $resumen = New Resumen(0);
        return view("admin.reports.docsAB", ["data" => $results, "resumen" => $resumen, "fecha1" => $fecha1, "fecha2" => $fecha2, "obra" => $obra]);

    }


    public function showEnvRecib(Request $req)
    {

        $data = $this->setFIlters();

        if ($req->has('_token')) { ///metodo post se hizo la busqueda

            $data = $this->getFilters($req, $data); ///valores de los filtros

            ///////para documentos x obra  o total
            if ($this->isObraSelected()) {
                $document = new Documento();
                $documentos = $this->filtersApply($document); ///aplicando filtros
            } else {
                $document = new DocumentoUnico();
                $documentos = $this->filtersApply2($document); ///aplicando filtros
            }
            //////////


            ////aplicando todos los filtros
            $data["docs"] = $documentos->get();

        }

        // dd($data);


        return view("admin.reports.envRecib", $data);
    }


    private function setFIlters()
    {
        $fase = ['' => 'Seleccionar'] + FaseProy::
            select(['CodFasPro', DB::raw(" (CodFasPro + ' - ' + DesFasPro) AS descripcion")
            ])->orderBy("DesFasPro")->lists('descripcion', 'CodFasPro')->all();


        $tipo = ['' => 'Seleccionar'] + TipoDocumento::
            select(['CodTipDoc', DB::raw("(CodTipDoc + ' - ' + DesTipDoc)  AS descripcion")
            ])->orderBy("DesTipDoc")->lists('descripcion', 'CodTipDoc')->all();


        $area = ['' => 'Seleccionar'] + Area::
            select(['CodAre', DB::raw("(CodAre + ' - ' + DesAre)  AS descripcion")
            ])->orderBy("DesAre")->lists('descripcion', 'CodAre')->all();


        $subarea = ['' => 'Seleccionar'] + SubArea::
            select(['CodSubAre', DB::raw("(CodSubAre + ' - ' + DesSubAre)  AS descripcion")
            ])->orderBy("descripcion")->lists('descripcion', 'CodSubAre')->all();


        $tramo = ['' => 'Seleccionar'] + Tramo::
            select(['CodTra', DB::raw("(CodTra + ' - ' + DesTra)  AS descripcion")
            ])->orderBy("DesTra")->lists('descripcion', 'CodTra')->all();


        $unid = ['' => 'Seleccionar'] + UnidConstr::
            select(['CodUniCon', DB::raw("(CodUniCon + ' - ' + DesUniCon)  AS descripcion")
            ])->orderBy("descripcion")->lists('descripcion', 'CodUniCon')->all();

        $persona = ['' => 'Seleccionar'] + Persona::
            select(['CodEnt', DB::raw("(CodEnt + ' - ' + NomEnt)  AS descripcion")
            ])->orderBy("descripcion")->lists('descripcion', 'CodEnt')->all();


        $estatus = ['' => 'Seleccionar'] + Estatus::lists('DesEst', 'DesEst')->all();

        /*

                $obra = ['' => 'Seleccionar'] + SubObra::
                    select(['CodSubObr', DB::raw("(CONVERT(nvarchar,CodSubObr) + ' - ' + DesSubObr)   AS descripcion")
                    ])->orderBy("descripcion")->where("CodObr", Session::get('OBRASELECT'))->lists('descripcion', 'CodSubObr')->all();*/


        $obra = ['' => 'Seleccionar'] + SubObra::
            select(['CodSubObr', DB::raw("(CONVERT(nvarchar,CodSubObr) + ' - ' + DesSubObr)  AS descripcion")
            ])->orderBy("DesSubObr")->lists('descripcion', 'CodSubObr')->all();


        ////condicion recibido / enviado
        $condicion = array("E" => "Enviado", "R" => "Recibido");

        $filters = array("fase" => $fase, "faseValue" => '', "tipo" => $tipo, "tipoValue" => '',
            "area" => $area, "areaValue" => '', "subarea" => $subarea, "subareaValue" => '',
            "condicion" => $condicion, "condValue" => '',
            "tramo" => $tramo, "tramoValue" => '', "unid" => $unid, "unidValue" => '', "persona" => $persona, "personaValue" => '',
            "estatus" => $estatus, "estatusValue" => '', "obra" => $obra, "obraValue" => '', "fecha1" => '', "fecha2" => '', "carta" => '');

        return $filters;
    }


    private function getFilters(Request $req, $data)
    {

        if ($req->has('fase')) {

            $this->filtersResult["fase"] = $req->fase;
            $data["faseValue"] = $req->fase;

        }

        if ($req->has('obra')) {
            $pref = 'obra';
            $this->filtersResult[$pref] = $req->obra;
            $data[$pref . "Value"] = $req->obra;

        }

        if ($req->has('persona')) {
            $pref = 'persona';
            $this->filtersResult[$pref] = $req->persona;
            $data[$pref . "Value"] = $req->persona;

        }


        if ($req->has('area')) {
            $pref = 'area';
            $this->filtersResult[$pref] = $req->area;
            $data[$pref . "Value"] = $req->area;

        }

        if ($req->has('subarea')) {
            $pref = 'subarea';
            $this->filtersResult[$pref] = $req->subarea;
            $data[$pref . "Value"] = $req->subarea;
        }

        if ($req->has('tramo')) {
            $pref = 'tramo';
            $this->filtersResult[$pref] = $req->tramo;
            $data[$pref . "Value"] = $req->tramo;

        }

        if ($req->has('unidad')) {
            $pref = 'unid';
            $this->filtersResult[$pref] = $req->unidad;
            $data[$pref . "Value"] = $req->unidad;

        }

        if ($req->has('tipo')) {
            $pref = 'tipo';
            $this->filtersResult[$pref] = $req->tipo;
            $data[$pref . "Value"] = $req->tipo;

        }

        if ($req->has('estatus')) {
            $pref = 'estatus';
            $this->filtersResult[$pref] = $req->estatus;
            $data[$pref . "Value"] = $req->estatus;

        }

        if ($req->has('fecha1')) {
            $pref = 'fecha1';
            $this->filtersResult[$pref] = $req->fecha1;
            $data[$pref] = $req->fecha1;

        }

        if ($req->has('fecha2')) {
            $pref = 'fecha2';
            $this->filtersResult[$pref] = $req->fecha2;
            $data[$pref] = $req->fecha2;

        }

        /////cod de la carta
        if ($req->has('carta')) {
            $pref = 'carta';
            $this->filtersResult[$pref] = $req->carta;
            $data[$pref] = $req->carta;

        }


        /////condicion env / rec
        if ($req->has('condicion')) {
            $pref = 'condicion';
            $this->filtersResult[$pref] = $req->condicion;
            $data["condValue"] = $req->condicion;

        }

        return $data;

    }


    /**setiando los filtros
     * @param Request $req
     */
    private function getFilters2(Request $req)
    {

        if ($req->has('fase')) {

            $this->filtersResult["fase"] = $req->fase;


        }

        if ($req->has('obra')) {
            $pref = 'obra';
            $this->filtersResult[$pref] = $req->obra;


        }

        if ($req->has('persona')) {
            $pref = 'persona';
            $this->filtersResult[$pref] = $req->persona;


        }


        if ($req->has('area')) {
            $pref = 'area';
            $this->filtersResult[$pref] = $req->area;


        }

        if ($req->has('subarea')) {
            $pref = 'subarea';
            $this->filtersResult[$pref] = $req->subarea;

        }

        if ($req->has('tramo')) {
            $pref = 'tramo';
            $this->filtersResult[$pref] = $req->tramo;


        }

        if ($req->has('unidad')) {
            $pref = 'unid';
            $this->filtersResult[$pref] = $req->unidad;


        }

        if ($req->has('tipo')) {
            $pref = 'tipo';
            $this->filtersResult[$pref] = $req->tipo;


        }

        if ($req->has('estatus')) {
            $pref = 'estatus';
            $this->filtersResult[$pref] = $req->estatus;


        }

        if ($req->has('fecha1')) {
            $pref = 'fecha1';
            $this->filtersResult[$pref] = $req->fecha1;


        }

        if ($req->has('fecha2')) {
            $pref = 'fecha2';
            $this->filtersResult[$pref] = $req->fecha2;


        }

        /////cod de la carta
        if ($req->has('carta')) {
            $pref = 'carta';
            $this->filtersResult[$pref] = $req->carta;


        }


        /////condicion env / rec
        if ($req->has('condicion')) {
            $pref = 'condicion';
            $this->filtersResult[$pref] = $req->condicion;


        }


    }


    private function filtersApply(Documento $document)
    {


        ///////si no se consultan todos los documentos unicos
        $document = $document->where('CodObr', '=', Session::get('OBRASELECT'));


        if (isset($this->filtersResult["fase"]))
            $document = $document->where('CodFasPro', '=', $this->filtersResult["fase"]);

        if (isset($this->filtersResult["tipo"]))
            $document = $document->where('CodTipDoc', '=', $this->filtersResult["tipo"]);

        if (isset($this->filtersResult["area"]))
            $document = $document->where('CodAre', '=', $this->filtersResult["area"]);

        if (isset($this->filtersResult["subarea"]))
            $document = $document->where('CodSubAre', '=', $this->filtersResult["subarea"]);

        if (isset($this->filtersResult["tramo"]))
            $document = $document->where('CodTra', '=', $this->filtersResult["tramo"]);

        if (isset($this->filtersResult["unid"]))
            $document = $document->where('CodUniCon', '=', $this->filtersResult["unid"]);

        if (isset($this->filtersResult["obra"]))
            $document = $document->where('CodSubObr', '=', $this->filtersResult["obra"]);


        /////filtros por el ultimo movimiento
        if (isset($this->filtersResult["persona"])) {
            $document = $document->whereRaw("(SELECT top 1
                                                            m.CodEnt
                                                            FROM
                                                            dbo.SGDMovimientos AS m
                                                            WHERE
                                                            m.CodDoc = SGDDocumentos.CodDoc
                                                            ORDER BY
                                                            m.CodMov DESC ) = ? ", [$this->filtersResult["persona"]]);
        }

        ///estatus del documento
        if (isset($this->filtersResult["estatus"])) {
            $document = $document->whereRaw("(SELECT top 1
                                                            m.EstDoc
                                                            FROM
                                                            dbo.SGDMovimientos AS m
                                                            WHERE
                                                            m.CodDoc = SGDDocumentos.CodDoc
                                                            ORDER BY
                                                            m.CodMov DESC ) = ? ", [$this->filtersResult["estatus"]]);
        }

        /////cod de la carta
        if (isset($this->filtersResult["carta"])) {
            $document = $document->whereRaw("(SELECT top 1
                                                            m.CorOri
                                                            FROM
                                                            dbo.SGDMovimientos AS m
                                                            WHERE
                                                            m.CodDoc = SGDDocumentos.CodDoc
                                                            ORDER BY
                                                            m.CodMov DESC ) = ? ", [$this->filtersResult["carta"]]);
        }

        /////fecha desde
        if (isset($this->filtersResult["fecha1"])) {
            $document = $document->whereRaw("(SELECT top 1
                                                            m.FecDoc
                                                            FROM
                                                            dbo.SGDMovimientos AS m
                                                            WHERE
                                                            m.CodDoc = SGDDocumentos.CodDoc
                                                            ORDER BY
                                                            m.CodMov DESC ) >= ? ", [$this->filtersResult["fecha1"]]);
        }


        /////fecha desde
        if (isset($this->filtersResult["fecha2"])) {
            $document = $document->whereRaw("(SELECT top 1
                                                            m.FecDoc
                                                            FROM
                                                            dbo.SGDMovimientos AS m
                                                            WHERE
                                                            m.CodDoc = SGDDocumentos.CodDoc
                                                            ORDER BY
                                                            m.CodMov DESC ) <= ? ", [$this->filtersResult["fecha2"]]);
        }


        /////condicion env o recibido
        if (isset($this->filtersResult["condicion"])) {
            $document = $document->whereRaw("(SELECT top 1
                                                            m.TipMov
                                                            FROM
                                                            dbo.SGDMovimientos AS m
                                                            WHERE
                                                            m.CodDoc = SGDDocumentos.CodDoc
                                                            ORDER BY
                                                            m.CodMov DESC ) = ? ", [$this->filtersResult["condicion"]]);
        }


        /*   $r = $document->toSql();
        dd($r);*/

        return $document;

    }


    private function filtersApply2(DocumentoUnico $document)
    {


        if (isset($this->filtersResult["fase"]))
            $document = $document->where('CodFasPro', '=', $this->filtersResult["fase"]);

        if (isset($this->filtersResult["tipo"]))
            $document = $document->where('CodTipDoc', '=', $this->filtersResult["tipo"]);

        if (isset($this->filtersResult["area"]))
            $document = $document->where('CodAre', '=', $this->filtersResult["area"]);

        if (isset($this->filtersResult["subarea"]))
            $document = $document->where('CodSubAre', '=', $this->filtersResult["subarea"]);

        if (isset($this->filtersResult["tramo"]))
            $document = $document->where('CodTra', '=', $this->filtersResult["tramo"]);

        if (isset($this->filtersResult["unid"]))
            $document = $document->where('CodUniCon', '=', $this->filtersResult["unid"]);

        if (isset($this->filtersResult["obra"]))
            $document = $document->where('CodSubObr', '=', $this->filtersResult["obra"]);


        /////filtros por el ultimo movimiento
        if (isset($this->filtersResult["persona"])) {
            $document = $document->whereRaw("(SELECT top 1
                                                            m.CodEnt
                                                            FROM
                                                            dbo.SGDMovimientos AS m
                                                            WHERE
                                                            m.CodDoc = [vw_documento_unico].CodDoc
                                                            ORDER BY
                                                            m.CodMov DESC ) = ? ", [$this->filtersResult["persona"]]);
        }

        ///estatus del documento
        if (isset($this->filtersResult["estatus"])) {
            $document = $document->whereRaw("(SELECT top 1
                                                            m.EstDoc
                                                            FROM
                                                            dbo.SGDMovimientos AS m
                                                            WHERE
                                                            m.CodDoc = [vw_documento_unico].CodDoc
                                                            ORDER BY
                                                            m.CodMov DESC ) = ? ", [$this->filtersResult["estatus"]]);
        }

        /////cod de la carta
        if (isset($this->filtersResult["carta"])) {
            $document = $document->whereRaw("(SELECT top 1
                                                            m.CorOri
                                                            FROM
                                                            dbo.SGDMovimientos AS m
                                                            WHERE
                                                            m.CodDoc = [vw_documento_unico].CodDoc
                                                            ORDER BY
                                                            m.CodMov DESC ) = ? ", [$this->filtersResult["carta"]]);
        }

        /////fecha desde
        if (isset($this->filtersResult["fecha1"])) {
            $document = $document->whereRaw("(SELECT top 1
                                                            m.FecDoc
                                                            FROM
                                                            dbo.SGDMovimientos AS m
                                                            WHERE
                                                            m.CodDoc = [vw_documento_unico].CodDoc
                                                            ORDER BY
                                                            m.CodMov DESC ) >= ? ", [$this->filtersResult["fecha1"]]);
        }


        /////fecha desde
        if (isset($this->filtersResult["fecha2"])) {
            $document = $document->whereRaw("(SELECT top 1
                                                            m.FecDoc
                                                            FROM
                                                            dbo.SGDMovimientos AS m
                                                            WHERE
                                                            m.CodDoc = [vw_documento_unico].CodDoc
                                                            ORDER BY
                                                            m.CodMov DESC ) <= ? ", [$this->filtersResult["fecha2"]]);
        }


        /////condicion env o recibido
        if (isset($this->filtersResult["condicion"])) {
            $document = $document->whereRaw("(SELECT top 1
                                                            m.TipMov
                                                            FROM
                                                            dbo.SGDMovimientos AS m
                                                            WHERE
                                                            m.CodDoc = [vw_documento_unico].CodDoc
                                                            ORDER BY
                                                            m.CodMov DESC ) = ? ", [$this->filtersResult["condicion"]]);
        }


        /*   $r = $document->toSql();
        dd($r);*/

        return $document;

    }


    /**exporta docs a excel
     * @param $name
     */
    private function docs2Excel($name)
    {

        Excel::create($name, function ($excel) {

            $excel->sheet('Documentos', function ($sheet) {

                $sheet->fromArray($this->docs2Excel);

            });
        })->export('xls');

    }


    public function movs2Excel($id)
    {

        $this->docId = $id;

        Excel::create("movs_documento_$id", function ($excel) {

            $excel->sheet('Movimientos', function ($sheet) {

                $movs = Movimiento::where("CodDoc", $this->docId)->get();

                $sheet->fromArray($movs);

            });
        })->export('xls');

    }


    /**
     * exporta reporte general a excel
     */
    public function general2Excel(Request $req)
    {

        $this->getFilters2($req); ///valores de los filtros


        ///////para documentos x obra  o total
        if ($this->isObraSelected()) {
            $document = new Documento();
            $temp = $this->filtersApply($document)->get(); ///aplicando filtros
        } else {
            $document = new DocumentoUnico();
            $temp = $this->filtersApply2($document)->get(); ///aplicando filtros
        }
        //////////


        ////vectores temporales
        $tempArray = array();
        $nuevo = array();

        ////seleccionando campos para excel
        foreach ($temp as $doc) {
            $tempArray["fase"] = $doc->CodFasPro;
            $tempArray["tipo"] = $doc->CodTipDoc;
            $tempArray["area"] = $doc->CodAre;
            $tempArray["subarea"] = $doc->CodSubAre;
            $tempArray["tramo"] = $doc->CodTra;
            $tempArray["unidad"] = $doc->CodUniCon;
            $tempArray["correlativo"] = $doc->NumCor;
            $tempArray["obra"] = $doc->subObra->DesSubObr;
            $tempArray["parte"] = $doc->ParObrDoc;
            $tempArray["titulo"] = $doc->TitDoc;
            $tempArray["objeto"] = $doc->ObjObr;

            try { ///porsia no hay movimentos

                $movs = $doc->getUltMov();
                $tempArray["rev"] = $movs->NumRev;
                $tempArray["estatus"] = $movs->EstDoc;

            } catch (\Exception $e) {
                $tempArray["rev"] = "";
                $tempArray["estatus"] = "";
                Log::error("el documento $doc->CodDoc no tiene movimientos");
            }


            $nuevo[] = $tempArray;

        }


        $this->docs2Excel = $nuevo;

        $this->docs2Excel("reporte_docs_general");

    }


    /**
     * exporta reporte enviado y recibido a excel
     */
    public function envrec2Excel(Request $req)
    {

        $this->getFilters2($req); ///valores de los filtros


        ///////para documentos x obra  o total
        if ($this->isObraSelected()) {
            $document = new Documento();
            $temp = $this->filtersApply($document)->get(); ///aplicando filtros
        } else {
            $document = new DocumentoUnico();
            $temp = $this->filtersApply2($document)->get(); ///aplicando filtros
        }
        //////////

        ////vectores temporales
        $tempArray = array();
        $nuevo = array();


        ////seleccionando campos para excel
        foreach ($temp as $doc) {
            $tempArray["codigo"] = $doc->getCodigo($doc);


            try { ///porsia no hay movimentos

                $movs = $doc->getUltMov();

                $tempArray["fecha"] = $movs->FecDoc;
                $tempArray["carta"] = $movs->CorOri;
                $estatus = $movs->EstDoc;

            } catch (\Exception $e) {
                $tempArray["fecha"] = "";
                $tempArray["carta"] = "";
                $estatus = "";
                Log::error("el documento $doc->CodDoc no tiene movimientos");
            }

            $tempArray["obra"] = $doc->subObra->DesSubObr;
            $tempArray["parte"] = $doc->ParObrDoc;
            $tempArray["titulo"] = $doc->TitDoc;
            $tempArray["objeto"] = $doc->ObjObr;
            $tempArray["estatus"] = $estatus;

            $nuevo[] = $tempArray;

        }


        $this->docs2Excel = $nuevo;


        $this->docs2Excel("reporte_docs_envrec");

    }


    /**exportar a excel
     * @param Request $req
     */
    public function totalObra2Excel(Request $req)
    {


        $this->getFilters2($req); ///valores de los filtros


        $fecha1 = isset($this->filtersResult["fecha1"]) ? $this->filtersResult["fecha1"] : NULL;
        $fecha2 = isset($this->filtersResult["fecha2"]) ? $this->filtersResult["fecha2"] : NULL;

        $resumen = new Resumen(0);


        if ($this->isObraSelected())
            $results = DB::select('EXEC [dbo].[sp_totalxobra] @codObr = ?, @fecha1 = ?, @fecha2 = ?', array(Session::get('OBRASELECT'), $fecha1, $fecha2));
        else
            $results = DB::select('EXEC [dbo].[sp_totalxobra_uni] @fecha1 = ?, @fecha2 = ?', array($fecha1, $fecha2));


        Excel::create('Documentos Totales x Obra', function ($excel) use ($results, $resumen) {

            $excel->sheet('documentos', function ($sheet) use ($results, $resumen) {

                $sheet->loadView('admin.excel.totalObra', array('data' => $results, "resumen" => $resumen));

            });

        })->download('xlsx');
    }


    public function docsObra2Excel(Request $req)
    {

        $this->getFilters2($req); ///valores de los filtros


        $fecha1 = isset($this->filtersResult["fecha1"]) ? $this->filtersResult["fecha1"] : NULL;
        $fecha2 = isset($this->filtersResult["fecha2"]) ? $this->filtersResult["fecha2"] : NULL;

        if ($this->isObraSelected())
            $results = DB::select('EXEC [dbo].[sp_docsxobra] @codObr = ?, @fecha1 = ?, @fecha2 = ?', array(Session::get('OBRASELECT'), $fecha1, $fecha2));
        else
            $results = DB::select('EXEC [dbo].[sp_docsxobra_uni]  @fecha1 = ?, @fecha2 = ?', array($fecha1, $fecha2));


        $resumen = New Resumen(0);


        Excel::create('Documentos x Obra', function ($excel) use ($results, $resumen) {

            $excel->sheet('documentos', function ($sheet) use ($results, $resumen) {

                $sheet->loadView('admin.excel.docsObra', array('data' => $results, "resumen" => $resumen));

            });

        })->download('xlsx');

    }


    public function docsAB2Excel(Request $req)
    {

        $this->getFilters2($req); ///valores de los filtros

        $fecha1 = isset($this->filtersResult["fecha1"]) ? $this->filtersResult["fecha1"] : NULL;
        $fecha2 = isset($this->filtersResult["fecha2"]) ? $this->filtersResult["fecha2"] : NULL;

        $obra = new Obra(); ////para calcular el detalle de los documentos A y B

        if ($this->isObraSelected())
            $results = DB::select('EXEC [dbo].[sp_docsab] @codObr = ?, @fecha1 = ?, @fecha2 = ?', array(Session::get('OBRASELECT'), $fecha1, $fecha2));
        else
            $results = DB::select('EXEC [dbo].[sp_docsab_uni] @fecha1 = ?, @fecha2 = ?', array($fecha1, $fecha2));

        $resumen = New Resumen(0);


        Excel::create('Documentos en AB', function ($excel) use ($results, $resumen, $obra, $fecha1, $fecha2) {

            $excel->sheet('documentos', function ($sheet) use ($results, $resumen, $obra, $fecha1, $fecha2) {

                $sheet->loadView('admin.excel.docsAB', array('data' => $results, "resumen" => $resumen, "fecha1" => $fecha1, "fecha2" => $fecha2, "obra" => $obra));

            });

        })->download('xlsx');

    }


    /**si hay una obra seleccionada
     * @return bool
     */
    public static function isObraSelected()
    {

        return (Session::get("OBRASELECT") != "00");
    }


}