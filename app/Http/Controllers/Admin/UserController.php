<?php
/**
 * Created by PhpStorm.
 * User: delimce
 * Date: 5/11/2015
 * Time: 9:07 AM
 */
namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\UserProfile;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Laravel\Lumen\Routing\Controller as BaseController;
use Log;
use Validator;

class UserController extends BaseController
{


    /**
     * the model instance
     * @var User
     */
    protected $session;

    /**
     * Create a new authentication controller instance.
     *
     * @param  Authenticator $auth
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('required:1'); ///id del modulo requerido

    }


    public function getDataById(Request $req)
    {
        $user = new User();
        $data = $user->find($req->id);

        return $data;
    }


    public function saveOrUpdate(Request $req)
    {

        //////////validation
        $validator = Validator::make($req->all(), [
            'nombre' => 'required',
            'apellido' => 'required',
            'identificacion' => 'required',
            'tlf1' => 'required',
            'email' => 'required',
            'tlf1' => 'required',
            'estatus' => 'required'

        ]);

        if ($validator->fails()) {
            return 'error';

        } else {

            try {

                DB::beginTransaction();
                // database queries here
                $model = new User();

                //////////condicion para editar
                if ($req->has('id')) {
                    $model = $model->find($req->id);
                } else {
                    $model->password = Hash::make($req->input("password")); ///hashing

                    ////validacion del email no este repetido
                    $validar = User::where("email", $req->email)->count();
                    if ($validar > 0) {

                        DB::commit();

                        return 'email'; ///arroja el error y sale del metodo

                    }


                }
                ////

                $model->nombre = $req->nombre;
                $model->apellido = $req->apellido;
                $model->identificacion = $req->identificacion;
                $model->tlf1 = $req->tlf1;
                $model->perfil_id = $req->perfil_id;
                $model->email = $req->email;
                $model->estatus = $req->estatus;

                $model->save(); ////edita/inserta aviso


                //////////condicion para enviar correo
                if (!$req->has('id')) {
                    //////envio de correo bienvenida al usuario
                    //  $this->welcomeEmail($model->nombre . ' ' . $model->apellido, $req->input("password"), $model->email);
                    ////////
                }
                ////


                DB::commit();


            } catch (Exception $e) {
                // Woopsy
                DB::rollback();
            }

        }

    }


    public function delete(Request $req)
    {
        try {

            $model = new User();
            $id = $req->input("id", 0);
            $model->destroy($id);

            $result = array("tipo" => "ok");

        } catch (\Illuminate\Database\QueryException $e) {
            $errorCode = $e->errorInfo[1];
            if ($errorCode == 547) {
                // houston, we have a duplicate entry problem
                $result = array("titulo" => "Error eliminando el usuario",
                    "mensaje" => "error el registro se encuentra en uso",
                    "tipo" => "error");

                Log::error($e->errorInfo[2]);

            }
        }

        return response()->json($result); /// json
    }


    public function password()
    {

        return view("admin.user.password");

    }


    public function userPassword($id)
    {

        $usuario = new User();
        $data = $usuario->find($id);

        return view("admin.user.usersPassword", ['data' => $data]);
    }


    public function changePassword2(Request $req)
    {

        //////////validation
        $validator = Validator::make($req->all(), [
            'id2' => 'required',
            'pass1' => 'required'

        ]);

        if ($validator->fails()) {
            $result = array("titulo" => "Error", "mensaje" => "parametros invalidos", "tipo" => "error");
        } else {
            $id = $req->id2;
            $model = User::find($id);
            $model->password = Hash::make($req->input("pass1")); ///hashing
            $model->save();

            ////////envio de correo
            $nombre = $req->input("nombre");
            $clave = $req->input("pass1");
            $email = $req->input("email");
            //  $this->sendPassword($nombre, $clave, $email);
            $result = array("titulo" => "Exito", "mensaje" => "la clave ha sido cambiada", "tipo" => "info");

        }

        return response()->json($result); /// json


    }


    private function sendPassword($nombre, $newPass, $email)
    {

        $data = array(
            'nombre' => $nombre,
            'clave' => $newPass
        );

        Mail::send('admin.emails.password', $data, function ($message) use ($nombre, $email) {

            $message->from(getenv('MAIL_FROM_ADDRESS'), getenv('MAIL_FROM_NAME'));

            $message->to($email, $nombre)->subject('Cambio de Password de usuario');

        });

    }


    public function users()
    {
        $users = new User();
        $data = $users->where("id", "<>", Session::get('DATAUSER')->id)->get(); ///todos menos el logueado
        $estatus = array("1" => "Activo", "0" => "Inactivo");
        $perfiles = UserProfile::lists('descripcion', 'id')->all();

        return view("admin.user.list", ['data' => $data, "estatus" => $estatus, "perfiles" => $perfiles]);

    }


    private function welcomeEmail($nombre, $pass, $email)
    {

        $data = array(
            'nombre' => $nombre,
            'clave' => $pass,
            'roll' => 'Usuario'
        );

        Mail::send('admin.emails.welcome1', $data, function ($message) use ($nombre, $email) {

            $message->from(getenv('MAIL_FROM_ADDRESS'), getenv('MAIL_FROM_NAME'));

            $message->to($email, $nombre)->subject('Nuevo Registro de Usuario en iclasificados.com');

        });


    }


}