<?php
/**
 * Created by PhpStorm.
 * User: delimce
 * Date: 5/11/2015
 * Time: 9:07 AM
 */
namespace App\Http\Controllers\Api;

use App\Libs\Api\RestApi;
use App\Models\Obra;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;

class UserController extends BaseController
{


    public function info()
    {
        return phpinfo();
    }


    /**metodo que permite el acceso al sistema requiere el 'email' y 'password' de la persona
     * @param Request $request
     */
    public function login(Request $request)
    {

        $dataInput = $request->only('email', 'password');
        $resp = new RestApi();

        $usuarios = new User();
        $datos = $usuarios->where('email', $dataInput["email"])->first();


        ///////comparacion con el password de la base de datos
        if (Hash::check($dataInput['password'], $datos["password"])) {
            // The passwords match...

            ////verificando que este activo
            if ($datos["estatus"] == 1) {


                /////modulos a los que puede acceder
                $modulos = $datos->profile->moduleSelected()->get();
                foreach ($modulos as $mod)
                    $mods[] = $mod->modulo_id;
                $datos["accesos"] = (isset($mods)) ? $mods : 0; ///trae los accesos sino 0
                ////////

                //////lista de obras
                $obras = Obra::where("Activo", "=", 1)->lists('CodObr', 'CodObr');
                $obras->put('00', "Todos");


                $request->session()->put('OBRAS', $obras); ////lista de obras
                $obraActual = ($datos->obra == "") ? 'CGC-01' : $datos->obra; ///obra seleccionada
                $request->session()->put('OBRASELECT', $obraActual); ////lista de obras


                $request->session()->put('DATAUSER', $datos); ///datos del usuario
                $resp->setContent($datos);

            } else {
                $resp->setError("El usuario se encuentra inactivo");
            }

        } else {
            ///it doesn't match
            $resp->setError("Usuario ó Clave inválidos");
        }

        return $resp->responseJson();

    }


    public function setObra(Request $req)
    {

        $usuario = new User();
        $user = $usuario->find($req->session()->get("DATAUSER")->id);
        $user->obra = $req->obra;
        $user->save(); ///guarda la obra por defecto para el usuario

        $req->session()->set('OBRASELECT', $req->obra); ////guarda la obra actual

        $obra = Obra::where("CodObr", $req->obra)->first();

        try {
            $req->session()->set('OBRANOMBRE', $obra->Descri); ////EL NOMBRE DE LA OBRA

        } catch (\Exception $e) {
            $req->session()->set('OBRANOMBRE', "Todos los contratos"); ////EL NOMBRE DE LA OBRA
        }

        return "";
    }

    public function newPass($word)
    {
        return Hash::make($word);
    }


}