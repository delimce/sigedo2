<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->welcome();
});

$app->get('/api/info', 'Api\UserController@info');

/*******************ROUTES DE ADMIN *****************/
$app->get('admin/login', [
    'as' => 'login', 'uses' => 'Admin\AccountController@showLogin'
]);

////sin permiso para acceder al modulo
$app->get('admin/unauthorized', [
    'as' => 'unauthorized', 'uses' => 'Admin\AccountController@showUnauth'
]);





//////micuenta
$app->get('admin', 'Admin\AccountController@main');
$app->post('admin/doLogin', 'Admin\AccountController@doLogin');
$app->get('admin/logout', 'Admin\AccountController@logout');
$app->get('admin/myAccount', 'Admin\AccountController@myAccount'); ///mi cuenta
$app->get('admin/password', 'Admin\AccountController@password'); ///cambiar clave
$app->post('admin/changepassword', 'Admin\AccountController@changePassword'); ///cambiar clave
$app->post('admin/edit', 'Admin\AccountController@editUser'); ///edit
//////usuarios
$app->post('admin/changepassword2', 'Admin\UserController@changePassword2'); ///cambiar clave usuario generico
$app->get('admin/user/password/{id}', 'Admin\UserController@userPassword'); ///form cambiar pass users
$app->get("admin/user/list", 'Admin\UserController@users'); ///lista de usuarios
$app->post("admin/user/saveorupdate", 'Admin\UserController@saveOrUpdate'); ///lista de usuarios
$app->post("admin/user/delete", 'Admin\UserController@delete'); ///lista de usuarios
$app->post("admin/user/getdatabyid", 'Admin\UserController@getDataById'); ///detalle de usuario
///////perfiles
$app->get("admin/profile/list", 'Admin\ProfileController@getList'); ///lista de usuarios
$app->post("admin/profile/saveorupdate", 'Admin\ProfileController@saveOrUpdate'); ///crear o editar
$app->post("admin/profile/delete", 'Admin\ProfileController@delete'); ///borrar
$app->post("admin/profile/getdatabyid", 'Admin\ProfileController@getDataById'); ///detalle de usuario
////////reportes
$app->get("admin/reports/general", 'Admin\ReportController@showGeneral'); ///reporte general get
$app->post("admin/reports/general", 'Admin\ReportController@showGeneral'); ///reporte general post
$app->get("admin/reports/movs/{codDoc}", 'Admin\ReportController@getMovs'); ///movimientos segun el documento
$app->get("admin/reports/totalObra", 'Admin\ReportController@showTotalxObra'); ///reporte totales por obra
$app->get("admin/reports/docsObra", 'Admin\ReportController@showDocsxObra'); ///reporte docs por obra
$app->get("admin/reports/envRecib", 'Admin\ReportController@showEnvRecib'); ///reporte docs por obra
$app->get("admin/reports/docsAB", 'Admin\ReportController@showDocsAB'); ///reporte docs A y B

///////gerencial
$app->get("admin/gerencial/est", 'Admin\GerencialController@showEst'); ///reporte general get
$app->post("admin/gerencial/saveEst",'Admin\GerencialController@saveDocsEst'); ///guardando datos reporte genrencial
$app->get("admin/gerencial/report",'Admin\GerencialController@showReport'); ///reporte genrencial


////////excel
$app->get('admin/reports/general/excel', 'Admin\ReportController@general2Excel');
$app->get('admin/reports/envrec/excel', 'Admin\ReportController@envrec2Excel');
$app->get('admin/reports/totalObra/excel', 'Admin\ReportController@totalObra2Excel');
$app->get('admin/reports/docsObra/excel', 'Admin\ReportController@docsObra2Excel');
$app->get('admin/reports/docsAB/excel', 'Admin\ReportController@docsAB2Excel');
$app->get('admin/gerencial/excel', 'Admin\GerencialController@gerencial2Excel');
$app->get('admin/reports/movs/excel/{id}', 'Admin\ReportController@movs2Excel');

/******************ROUTES PARA LA API****************/

/////////****usuarios
$app->post("/api/user/login", 'Api\UserController@login');
$app->post("/api/user/setobra",'Api\UserController@setObra');
$app->get("/api/newPass/{word}",'Api\UserController@newPass');