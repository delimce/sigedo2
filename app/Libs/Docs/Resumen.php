<?php
/**
 * Created by PhpStorm.
 * User: delimce
 * Date: 19/1/2016
 * Time: 12:33 PM
 */

namespace App\Libs\Docs;


class Resumen
{

    private $totalStatus;
    private $totalTipo;
    private $totalDocs;
    public $gerencial;

    private $gerencialTPGT;
    private $gerencialTAPY;
    private $gerencialTGNRL;
    private $gerencialTPP;


    /**
     * Resumen constructor.
     * @param array $totalStatus
     */
    public function __construct($total)
    {
        $this->totalStatus = array("docs" => 0, "A" => 0, "B" => 0, "C" => 0, "SE" => 0, "NA" => 0, "blo" => 0);
        $this->totalTipo = array("pl" => 0, "cr" => 0, "md" => 0, "cm" => 0, "ca" => 0, "es" => 0, "inf" => 0, "esq" => 0, "otros" => 0);
        $this->gerencial = array("ttdosc" => 0, "testima" => 0, "testa" => 0, "testb" => 0, "testc" => 0,
            "testse" => 0, "tgt" => 0, "testba" => 0, "testblo" => 0, "tcm" => 0, "tes" => 0, "tpl" => 0, "tmd" => 0, "ttotal" => 0);
        $this->totalDocs = $total;
    }


    /**
     * @return mixed
     */
    public function getTotalDocs()
    {
        return $this->totalDocs;
    }

    /**
     * @param mixed $totalDocs
     */
    public function setTotalDocs($totalDocs)
    {
        $this->totalDocs = $totalDocs;
    }


    /**
     * total por estatus
     */
    public function setTotalStatus($estatus)
    {

        switch ($estatus) {
            case 'A':
                $this->totalStatus["A"]++;
                break;
            case 'B':
                $this->totalStatus["B"]++;
                break;
            case 'C':
                $this->totalStatus["C"]++;
                break;
            case 'S/E':
                $this->totalStatus["SE"]++;
                break;
            case 'N/A':
                $this->totalStatus["NA"]++;
                break;
            default:
                $this->totalStatus["blo"]++;
                break;
        }

    }


    /**
     * set total tipo
     */
    public function setTotalTipo($tipo,$total)
    {

        switch ($tipo) {
            case 'pl':
                $this->totalTipo["pl"]+=$total;
                break;
            case 'cr':
                $this->totalTipo["cr"]+=$total;
                break;
            case 'md':
                $this->totalTipo["md"]+=$total;
                break;
            case 'cm':
                $this->totalTipo["cm"]+=$total;
                break;
            case 'ca':
                $this->totalTipo["ca"]+=$total;
                break;
            case 'es':
                $this->totalTipo["es"]+=$total;
                break;
            case 'inf':
                $this->totalTipo["inf"]+=$total;
                break;
            case 'esq':
                $this->totalTipo["esq"]+=$total;
                break;
            case 'otros':
                $this->totalTipo["otros"]+=$total;
                break;
        }

    }


    public function resetDocs()
    {
        $this->totalStatus = array("docs" => 0, "A" => 0, "B" => 0, "C" => 0, "SE" => 0, "NA" => 0, "blo" => 0);
    }

    public function countDocs($cant)
    {
        $this->totalStatus["docs"] += $cant;
    }

    public function countA($cant)
    {
        $this->totalStatus["A"] += $cant;
    }

    public function countB($cant)
    {
        $this->totalStatus["B"] += $cant;
    }

    public function countC($cant)
    {
        $this->totalStatus["C"] += $cant;
    }

    public function countSE($cant)
    {
        $this->totalStatus["SE"] += $cant;
    }

    public function countNA($cant)
    {
        $this->totalStatus["NA"] += $cant;
    }

    public function countBlo($cant)
    {
        $this->totalStatus["blo"] += $cant;
    }

    public function getTotalStatusWhole($item)
    {
        return $item->estatus_a + $item->estatus_b + $item->estatus_c + $item->estatus_se + $item->estatus_na + $item->estatus_blo;
    }


    /**
     * @return array
     */
    public function getTotalStatus($estatus)
    {
        return $this->totalStatus[$estatus];
    }


    /**total tipo
     * @param $tipo
     * @return mixed
     */
    public function getTotalTipo($tipo)
    {
        return $this->totalTipo[$tipo];
    }





    /**********************************************
     * metodos para el reporte gerencial
     */



    /**
     * @return mixed
     */
    public function getGerencialTPGT()
    {
        return $this->gerencialTPGT;
    }

    /**
     * @param mixed $gerencialTPGT
     */
    public function setGerencialTPGT($gerencialTPGT)
    {
        $this->gerencialTPGT = $gerencialTPGT;
    }

    /**
     * @return mixed
     */
    public function getGerencialTAPY()
    {
        return $this->gerencialTAPY;
    }

    /**
     * @param mixed $gerencialTAPY
     */
    public function setGerencialTAPY($gerencialTAPY)
    {
        $this->gerencialTAPY = $gerencialTAPY;
    }

    /**
     * @return mixed
     */
    public function getGerencialTGNRL()
    {
        return $this->gerencialTGNRL;
    }

    /**
     * @param mixed $gerencialTGNRL
     */
    public function setGerencialTGNRL($gerencialTGNRL)
    {
        $this->gerencialTGNRL = $gerencialTGNRL;
    }

    /**
     * @return mixed
     */
    public function getGerencialTPP()
    {
        return $this->gerencialTPP;
    }

    /**
     * @param mixed $gerencialTPP
     */
    public function setGerencialTPP($gerencialTPP)
    {
        $this->gerencialTPP = $gerencialTPP;
    }






}