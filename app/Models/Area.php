<?php
namespace App\Models;

use App\Quotation;
use DB;
use Illuminate\Database\Eloquent\Model;

final class Area extends Model
{

    protected $table = 'SGDArea';

    protected $primaryKey ="CodAre";

    public function getDesAreAttribute($value)
    {
        return utf8_encode($value);
    }

    public function getDescripcionAttribute($value)
    {
        return utf8_encode($value);
    }

    public function getDates()
    {
        return array();
    }


}