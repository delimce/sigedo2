<?php
namespace App\Models;

use App\Quotation;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

 class Documento extends Model
{

    protected $table = 'SGDDocumentos';
    protected $primaryKey = "CodDoc";

    private $movs = array("rev" => "", "status" => "", "date" => "", "carta" => "", "person" => ""); ////ult movimientos
    private $submovs = array("rev" => "", "status" => ""); ///penultimos movimientos


    ////foreing key
    public function movimiento()
    {
        return $this->hasMany('App\Models\Movimiento', 'CodDoc');
    }


    ///foreing key
    public function compania()
    {
        return $this->belongsTo('App\Models\Compania', 'CodCom');
    }


    ///foreing key
    public function obra()
    {
        return $this->belongsTo('App\Models\Obra', 'CodObr');
    }

    ///foreing key
    public function faseProy()
    {
        return $this->belongsTo('App\Models\FaseProy', 'CodFasPro');
    }


    ///foreing key
    public function tipo()
    {
        return $this->belongsTo('App\Models\TipoDocumento', 'CodTipDoc');
    }

    ///foreing key
    public function area()
    {
        return $this->belongsTo('App\Models\Area', 'CodAre');
    }

    ///foreing key
    public function subarea()
    {
        return $this->belongsTo('App\Models\SubArea', 'CodSubAre');
    }

    ///foreing key
    public function tramo()
    {
        return $this->belongsTo('App\Models\Tramo', 'CodTra');
    }


    ///foreing key
    public function unidadConstr()
    {
        return $this->belongsTo('App\Models\UnidConstr', 'CodUniCon');
    }

    ///foreing key
    public function subObra()
    {
        return $this->belongsTo('App\Models\SubObra', 'CodSubObr');
    }


    /*************metodos y funciones*********/////


    public function getCodigo($doc)
    {
        return $doc->CodCom . ' ' . $doc->CodFasPro . ' ' . $doc->CodTipDoc . ' ' .
        $doc->CodAre . ' ' . $doc->CodSubAre . ' ' . $doc->CodTra . ' ' . $doc->CodUniCon . ' ' . $doc->NumCor;
    }

    /**trae ultimo movimiento
     * @return mixed
     */
    public function getUltMov()
    {
        $mov = $this->movimiento()->orderBy("CodMov", "desc")->first();
        if (count($mov) > 0) {
            $this->movs["date"] =  $mov->FecDoc;
            $this->movs["carta"] = $mov->CorOri;
            $this->movs["rev"] = $mov->NumRev;
            $this->movs["status"] = $mov->EstDoc;
            try {
                $this->movs["person"] = $mov->persona->NomEnt;
            } catch (\ErrorException $e) {
                $this->movs["person"] = "";
                log::info("no existe persona asociada para el movimiento: $mov->CodMov");
            }

        }

        return $mov;

    }

    /**
     * trae el penuntimo movimiento
     */
    public function getPenUltMov()
    {

        $mov = $this->movimiento()->orderBy("CodMov", "desc")->take(1)->skip(1)->first();
        if (count($mov) > 0) {
            $this->submovs["rev"] = $mov->NumRev;
            $this->submovs["status"] = $mov->EstDoc;
        }

        return $mov;


    }

    /**
     * @return array
     */
    public function getSubmovs($key)
    {
        return $this->submovs[$key];
    }

    /**
     * @return array
     */
    public function getMovs($key)
    {
        return $this->movs[$key];
    }


    public function getDates()
    {
        return array();
    }


}