<?php
namespace App\Models;

use App\Quotation;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

final class DocumentoUnico extends Documento
{

    protected $table = 'vw_documento_unico'; ///vista con documentos unicos vw_documento_unico
    protected $primaryKey = "CodDoc";



}