<?php
namespace App\Models;

use App\Quotation;
use DB;
use Illuminate\Database\Eloquent\Model;

final class FaseProy extends Model
{

    protected $table = 'SGDFaseProy';

    protected $primaryKey ="CodFasPro";

    public function getDescripcionAttribute($value)
    {
        return utf8_encode($value);
    }


    public function getDates()
    {
        return array();
    }


}