<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Quotation;

final class Gerencial extends Model
{

    protected $table = 'tbl_gerencial_est';


    public function getDates()
    {
        return array();
    }



    ///foreing key
    public function contrato()
    {
        return $this->belongsTo('App\Models\Obra','CodObr');
    }

    ///foreing key
    public function obra()
    {
        return $this->belongsTo('App\Models\SubObra','CodSubObr');
    }




}