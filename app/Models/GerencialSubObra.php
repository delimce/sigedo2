<?php
namespace App\Models;

use App\Quotation;
use DB;
use Illuminate\Database\Eloquent\Model;

final class GerencialSubObra extends Model
{

    protected $table = 'tbl_gerencial_subobra';

    protected $primaryKey ="CodSubObr";

    public function getDesSubObrAttribute($value)
    {
        return utf8_encode($value);
    }

    public function getDescripcionAttribute($value)
    {
        return utf8_encode($value);
    }


    public function getDates()
    {
        return array();
    }


}