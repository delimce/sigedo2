<?php
namespace App\Models;

use App\Quotation;
use DB;
use Illuminate\Database\Eloquent\Model;

final class Module extends Model
{

    protected $table = 'tbl_modulo';


    ////foreing key
    public function profile()
    {
        return $this->belongsToMany('App\Models\UserProfile', 'tbl_usuario_perfil_modulo', 'modulo_id', 'perfil_id');
    }


    public function getDates()
    {
        return array();
    }


}