<?php
namespace App\Models;

use App\Quotation;
use DB;
use Illuminate\Database\Eloquent\Model;

final class Movimiento extends Model
{

    protected $table = 'SGDMovimientos';

    protected $primaryKey = "CodMov";


    ///foreing key
    public function documento()
    {
        return $this->belongsTo('App\Models\Documento', 'CodDoc');
    }

    ///foreing key
    public function persona()
    {
        return $this->belongsTo('App\Models\Persona', 'CodEnt');
    }


    public function getFecDocAttribute($value)
    {
        return date("d/m/Y", strtotime($value));
    }


    public function getDates()
    {
        return array();
    }


}