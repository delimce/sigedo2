<?php
namespace App\Models;

use App\Http\Controllers\Admin\ReportController;
use App\Quotation;
use DB;
use Illuminate\Database\Eloquent\Model;
use Session;

final class Obra extends Model
{


    private $subObra;
    private $fecha1;
    private $fecha2;


    protected $table = 'SGDObras';

    protected $primaryKey = "IdDoc";


    public function documento()
    {
        return $this->hasMany('App\Models\Documento', 'CodDoc');
    }


    public function getDescriAttribute($value)
    {
        return utf8_encode($value);
    }

    public function getDescripcionAttribute($value)
    {
        return utf8_encode($value);
    }


    public function getDates()
    {
        return array();
    }


    /**
     * @return mixed
     */
    public function getSubObra()
    {
        return $this->subObra;
    }

    /**
     * @param mixed $subObra
     */
    public function setSubObra($subObra)
    {
        $this->subObra = $subObra;
    }

    /**
     * @return mixed
     */
    public function getFecha1()
    {
        return $this->fecha1;
    }

    /**
     * @param mixed $fecha1
     */
    public function setFecha1($fecha1)
    {
        $this->fecha1 = $fecha1;
    }

    /**
     * @return mixed
     */
    public function getFecha2()
    {
        return $this->fecha2;
    }

    /**
     * @param mixed $fecha2
     */
    public function setFecha2($fecha2)
    {
        $this->fecha2 = $fecha2;
    }


    /**regresa el detalle de los documentos AB dada la subObra y fecha 1 y fecha2
     * @return mixed
     */
    public function getDocsABDetail()
    {


        if ( ReportController::isObraSelected())
            $results = DB::select('EXEC [dbo].[sp_docsab_detail] @codObr = ?, @CodSubObr = ?, @fecha1 = ?, @fecha2 = ?', array(Session::get('OBRASELECT'), $this->subObra, $this->fecha1, $this->fecha2));
        else
            $results = DB::select('EXEC [dbo].[sp_docsab_uni_detail] @CodSubObr = ?, @fecha1 = ?, @fecha2 = ?', array($this->subObra, $this->fecha1, $this->fecha2));


        return $results;

    }


}