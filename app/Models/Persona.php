<?php
namespace App\Models;

use App\Quotation;
use DB;
use Illuminate\Database\Eloquent\Model;

final class Persona extends Model
{

    protected $table = 'SGDEntes';

    protected $primaryKey ="CodEnt";


    public function getDescripcionAttribute($value)
    {
        return utf8_encode($value);
    }

    public function getNomEntAttribute($value)
    {
        return utf8_encode($value);
    }


    public function getDates()
    {
        return array();
    }


}