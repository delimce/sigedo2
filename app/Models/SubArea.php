<?php
namespace App\Models;

use App\Quotation;
use DB;
use Illuminate\Database\Eloquent\Model;

final class SubArea extends Model
{

    protected $table = 'SGDSubArea';

    protected $primaryKey ="CodSubAre";

    public function getDesSubAreAttribute($value)
    {
        return utf8_encode($value);
    }


    public function getDescripcionAttribute($value)
    {
        return utf8_encode($value);
    }


    public function getDates()
    {
        return array();
    }


}