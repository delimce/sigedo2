<?php
namespace App\Models;

use App\Quotation;
use DB;
use Illuminate\Database\Eloquent\Model;

final class TipoDocumento extends Model
{

    protected $table = 'SGDTipoDocu';

    protected $primaryKey ="CodTipDoc";


    public function getDesTipDocAttribute($value)
    {
       return utf8_encode($value);
    }

    public function getDescripcionAttribute($value)
    {
        return utf8_encode($value);
    }


    public function getDates()
    {
        return array();
    }


}