<?php
namespace App\Models;

use App\Quotation;
use DB;
use Illuminate\Database\Eloquent\Model;

final class Tramo extends Model
{

    protected $table = 'SGDTramo';

    protected $primaryKey ="CodTra";

    public function getDesTraAttribute($value)
    {
        return utf8_encode($value);
    }

    public function getDescripcionAttribute($value)
    {
        return utf8_encode($value);
    }


    public function getDates()
    {
        return array();
    }


}