<?php
namespace App\Models;

use App\Quotation;
use DB;
use Illuminate\Database\Eloquent\Model;

final class UnidConstr extends Model
{

    protected $table = 'SGDUnidCons';

    protected $primaryKey ="CodUniCon";

    public function getDesUniConAttribute($value)
    {
        return utf8_encode($value);
    }

    public function getDescripcionAttribute($value)
    {
        return utf8_encode($value);
    }


    public function getDates()
    {
        return array();
    }


}