<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Quotation;

final class User extends Model
{

    protected $table = 'tbl_usuario';


    public function getDates()
    {
        return array();
    }



    ///foreing key
    public function profile()
    {
        return $this->belongsTo('App\Models\UserProfile','perfil_id');
    }



    public function getUserData($id)
    {
        return DB::table('tbl_usuario as u')
            ->join('tbl_usuario_perfil as p', 'p.id', '=', 'u.perfil_id')
            ->where('u.id', '=', $id)
            ->select('u.id', 'u.nombre', 'u.apellido','u.identificacion','u.email','u.tlf1',
                'p.descripcion as perfil')->first(); ////1 registro
    }


}