<?php
namespace App\Models;

use App\Quotation;
use DB;
use Illuminate\Database\Eloquent\Model;

final class UserProfile extends Model
{

    protected $table = 'tbl_usuario_perfil';

    ////foreing key
    public function user()
    {
        return $this->hasMany('App\Models\User', 'perfil_id');
    }


    ////foreing key
    public function module()
    {
        return $this->belongsToMany('App\Models\Module', 'tbl_usuario_perfil_modulo', 'perfil_id', 'modulo_id');
    }

    public function moduleSelected()
    {
        return $this->hasMany('App\Models\UserProfileModule', 'perfil_id');
    }


    public function getDates()
    {
        return array();
    }

}