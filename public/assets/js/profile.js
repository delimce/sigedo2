/**
 * Created by delimce on 25/12/2015.
 */
/////ajusta ventana y datos dinamicamente
var goModule = function (ide) {

    if ($("#id").length)  $('#id').remove();

    if (ide == 0) { ///insertar

        $("#save").html("Crear");
        $("#form1").trigger('reset');

    } else { //editar
        $("#form1").trigger('reset');
        $("#save").html("Editar");
        $('<input>').attr({
            type: 'hidden',
            id: 'id',
            name: 'id',
            value: ide
        }).appendTo('#form1');
        getDataById(ide);
    }

}


///trae los datos para editar
var getDataById = function (ide) {

    jQuery.ajax({
        url: PATHAPP + 'admin/profile/getdatabyid',
        type: 'POST',
        data: {"id": ide},
        success: function (response) {

            ///llenar los datos solicitados
            $("#nombre").val(response.descripcion);

            var modulos = response.modulos;

            var mods = modulos.split(',');


            $("form input:checkbox").each(function () {

                if ($.inArray($(this).val(), mods) != -1) {
                    $(this).prop("checked", true);
                }
            });

        }

    });

}


var saveOrUpdate = function () {
    ////entrada de datos
    var form = $('#form1');

    jQuery.ajax({
        url: PATHAPP + 'admin/profile/saveorupdate',
        type: 'POST',
        data: form.serialize(),
        beforeSend: function () {
            $("#save").html("Enviando...");
            $('#save').attr('disabled', 'disabled');
        },
        success: function (response) {
            // response

            $("#save").html("Guardar");
            $('#save').removeAttr("disabled");

            if (response == 'error') {
                myAlert("Ha ocurrido un error", "Los datos suministrados por ud se encuentran " +
                    "incompletos, por favor, verifique que completó todos los campos " +
                    "obligatorios.", 'error');
            } else {
                var goto = PATHAPP + 'admin/profile/list';
                location.replace(goto);
            }


        },

    });


}


var deletePro = function (id) {
    $.confirm({
        text: "Esta seguro que desea borrar este Perfil de usuario?",
        title: "Borrar Perfil de usuario",
        confirm: function () {
            jQuery.ajax({
                url: PATHAPP + 'admin/profile/delete',
                type: 'POST',
                data: {"id": id},
                success: function (response) {

                    if (response.tipo == 'ok')
                        location.reload();
                    else
                        myAlert(response.titulo, response.mensaje, response.tipo);

                }

            });
        },
        cancel: function (button) {
            // nothing to do
        },
        confirmButton: "Si, lo estoy",
        cancelButton: "No",
        post: true,
        confirmButtonClass: "btn-danger",
        cancelButtonClass: "btn-default",
        dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
    });

}

