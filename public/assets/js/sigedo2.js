/**
 * Created by delimce on 13/3/2016.
 */

var saveEst = function () {
    ////entrada de datos
    var form = $('#form1');

    jQuery.ajax({
        url: PATHAPP + 'admin/gerencial/saveEst',
        type: 'POST',
        data: form.serialize(),
        beforeSend: function () {
            $("#save").html("Enviando...");
            $('#save').attr('disabled', 'disabled');
        },
        success: function (response) {
            // response

            $("#save").html("Guardar");
            $('#save').removeAttr("disabled");


            BootstrapDialog.show({
                title: "Información",
                message: "Datos guardados con exito"
            });


        },

    });


}