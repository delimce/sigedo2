/**
 * Created by delimce on 10/11/2015.
 */

var userLogin = function () {

    ////entrada de datos
    var form = $('#form1');


    jQuery.ajax({
        url: PATHAPP + 'api/user/login',
        type: 'POST',
        dataType: 'json',
        data: form.serialize(),
        /*        beforeSend: function (xhr) {
         // xhr.setRequestHeader( "Authorization", "BEARER " + access_token );
         },*/
        beforeSend: function () {
            $("#message").html("<p class='text-center'><img src='" + PATHAPP + "/assets/images/ajax-loader.gif'></p>")
        },
        success: function (response) {
            // response
            var msg = "";
            if (response.success == 'false') { ///nolog
                msg = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + response.error + '</div>';
                $("#message").html(msg);
            } else { ///loging
                var adminurl = PATHAPP + 'admin';
                location.replace(adminurl);
            }

        }
    });

};


var userEdit = function () {
    ////entrada de datos
    var form = $('#form1');

    jQuery.ajax({
        url: PATHAPP + 'admin/edit',
        type: 'POST',
        data: form.serialize(),
        beforeSend: function () {
            $("#save").html("Enviando...");
            $('#save').attr('disabled', 'disabled');
        },
        success: function (response) {
            // response

            $("#save").html("Guardar");
            $('#save').removeAttr("disabled");

            $('#userfullname').html(response.nombre + ' ' + response.apellido); ///setiando nombre

            BootstrapDialog.show({
                title: response.titulo,
                message: response.mensaje
            });


        },

    });


}


var changePass = function (ide) {

    jQuery.ajax({
        url: PATHAPP + 'admin/user/getdatabyid',
        type: 'POST',
        data: {"id": ide},
        beforeSend: function () {
            $("#save").html("Enviando...");
            $('#save').attr('disabled', 'disabled');
        },
        success: function (response) {
            // response

            $("#nombre2").val(response.nombre + ' ' + response.apellido);
            $("#email2").val(response.email);
            $("#id2").val(response.id);

        },

    });

}


/////ajusta ventana y datos dinamicamente
var goModule = function (ide) {

    if ($("#id").length)  $('#id').remove();

    if (ide == 0) { ///insertar

        $("#save").html("Crear");
        $("#form1").trigger('reset');
        $("#passblock").show(); ///ocultando claves

    } else { //editar
        $("#save").html("Editar");
        $('<input>').attr({
            type: 'hidden',
            id: 'id',
            name: 'id',
            value: ide
        }).appendTo('#form1');
        getDataById(ide);
    }

}


///trae los datos para editar
var getDataById = function (ide) {

    jQuery.ajax({
        url: PATHAPP + 'admin/user/getdatabyid',
        type: 'POST',
        data: {"id": ide},
        success: function (response) {

            ///llenar los datos solicitados
            $("#passblock").hide(); ///ocultando claves
            $("#password").val('noclave123');
            $("#pass2").val('noclave123');
            $("#nombre").val(response.nombre);
            $("#apellido").val(response.apellido);
            $("#identificacion").val(response.identificacion);
            $("#perfil_id").val(response.perfil_id);
            $("#email").val(response.email);
            $("#tlf1").val(response.tlf1);
            $("#estatus").val(response.estatus);

        }

    });

}


var saveOrUpdate = function () {
    ////entrada de datos
    var form = $('#form1');

    jQuery.ajax({
        url: PATHAPP + 'admin/user/saveorupdate',
        type: 'POST',
        data: form.serialize(),
        beforeSend: function () {
            $("#save").html("Enviando...");
            $('#save').attr('disabled', 'disabled');
        },
        success: function (response) {
            // response

            $("#save").html("Guardar");
            $('#save').removeAttr("disabled");

            if (response == 'error') {
                myAlert("Ha ocurrido un error", "Los datos suministrados por ud se encuentran " +
                    "incompletos, por favor, verifique que completó todos los campos " +
                    "obligatorios.", 'error');
            } else if (response == 'email') {

                myAlert("El Email ya existe", "El correo electronico ingresado " +
                    "ya se encuentra registrado", 'error');

            } else {
                var goto = PATHAPP + 'admin/user/list';
                location.replace(goto);
            }


        },

    });


}


var deleteUser = function (id) {
    $.confirm({
        text: "Esta seguro que desea borrar esta persona?",
        title: "Borrar Usuario",
        confirm: function () {
            jQuery.ajax({
                url: PATHAPP + 'admin/user/delete',
                type: 'POST',
                data: {"id": id},
                success: function (response) {

                    if (response.tipo == 'ok')
                        location.reload();
                    else
                        myAlert(response.titulo, response.mensaje, response.tipo);

                }

            });
        },
        cancel: function (button) {
            // nothing to do
        },
        confirmButton: "Si, lo estoy",
        cancelButton: "No",
        post: true,
        confirmButtonClass: "btn-danger",
        cancelButtonClass: "btn-default",
        dialogClass: "modal-dialog modal-lg" // Bootstrap classes for large modal
    });

}


var changePassword = function () {

    ////entrada de datos
    var form = $('#form1');

    jQuery.ajax({
        url: PATHAPP + 'admin/changepassword',
        type: 'POST',
        data: form.serialize(),
        beforeSend: function () {
            $("#save").html("Enviando...");
            $('#save').attr('disabled', 'disabled');
        },
        success: function (response) {
            // response

            $("#save").html("Guardar");
            $('#save').removeAttr("disabled");
            /////notificacion de operacion
            myAlert(response.titulo, response.mensaje, response.tipo);


        },

    });

}


var changePassword2 = function () {

    ////entrada de datos
    var form = $('#form2');

    jQuery.ajax({
        url: PATHAPP + 'admin/changepassword2',
        type: 'POST',
        data: form.serialize(),
        beforeSend: function () {
            $("#save2").html("Enviando...");
            $('#save2').attr('disabled', 'disabled');
        },
        success: function (response) {
            // response

            $("#save2").html("Guardar");
            $('#save2').removeAttr("disabled");
            /////notificacion de operacion
            myAlert(response.titulo, response.mensaje, response.tipo);


        },

    });

}

