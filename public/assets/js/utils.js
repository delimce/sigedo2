/**
 * Created by delimce on 29/11/2015.
 */

var myAlert = function (title,msg,type) {


    var tipo;
    switch(type) {
        case 'info':
            tipo = BootstrapDialog.TYPE_INFO;
            break;
        case 'warning':
            tipo = BootstrapDialog.TYPE_WARNING;
            break;
        case 'error':
            tipo = BootstrapDialog.TYPE_DANGER;
            break;
        default:
            tipo = BootstrapDialog.TYPE_SUCCESS;

    }

    BootstrapDialog.show({
        title: title,
        message: msg,
        type: tipo
    });

}
