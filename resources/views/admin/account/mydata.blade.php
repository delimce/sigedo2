@extends('layout.main')
@section('title', 'Mis Datos')
@section('content')
    <div class="col-lg-8">
        <form name="form1" id="form1" role="form" data-toggle="validator">
            <div class="form-group">
                <label>Nombre</label>
                <input name="nombre" value="{{$data->nombre}}" class="form-control" required>
            </div>
            <div class="form-group">
                <label>Apellido</label>
                <input name="apellido" value="{{$data->apellido}}" class="form-control" placeholder="Enter text" required>
            </div>
            <div class="form-group">
                <label>Cedula</label>
                <input name="cedula" value="{{$data->identificacion}}" class="form-control" placeholder="Enter text" required>
            </div>
            <div class="form-group">
                <label>email</label>
                <input name="email" type="email" value="{{$data->email}}" class="form-control" placeholder="Enter text" required>
            </div>
            <div class="form-group">
                <label>Telefono 1</label>
                <input name="tlf1" type="number" value="{{$data->tlf1}}" class="form-control" placeholder="Enter text">
            </div>

            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <button id="save" type="submit" class="btn btn-primary">Guardar</button>
            <button type="reset" class="btn btn-default">Borrar</button>
        </form>
    </div>
@endsection


@section('foot')
    {!! Html::script('assets/js/user.js') !!}

    <script>
        $("#form1").submit(function(e) {
            userEdit();
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

    </script>

@stop