@extends('layout.main')
@section('title', 'Cambio de clave')
@section('head')
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
@stop
@section('content')
    <div class="col-lg-8">
        <form name="form1" id="form1" role="form" data-toggle="validator">

            <div class="form-group">
                <label>Ingrese la clave Actual</label>
                <input name="mypass" id="mypass" data-minlength="3" type="password" class="form-control" placeholder="coloque la clave" required>
            </div>

            <div class="form-group">
                <label>Ingrese la nueva clave (Mayor a 5 caracteres)</label>
                <input name="pass1" id="pass1" data-minlength="6" type="password" class="form-control" placeholder="coloque una clave" required>
            </div>
            <div class="form-group">
                 <input name="pass2" id="pass2" data-minlength="6" type="password" class="form-control" placeholder="repita la clave"
                        data-match="#pass1" data-match-error="Las claves no coinciden" required>
                <div class="help-block with-errors"></div>
            </div>

            <button id="save" type="submit"  class="btn btn-primary">Guardar</button>
        </form>

    </div>

@endsection


@section('foot')

    {!! Html::script('assets/js/utils.js') !!}
    {!! Html::script('assets/js/user.js') !!}

    <script>
        $("#form1").submit(function(e) {
            changePassword();
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

    </script>

@stop