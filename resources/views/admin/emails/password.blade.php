@extends('layout.email')
@section('title', 'Cambio de Password')
{{-- abrir el contenido--}}
@section('content')

    <div style="text-align: justify">
        Buen dia Sr {!! $nombre !!} ud ha modificado su clave con éxito
        <p>Su nueva clave es: <strong>{!! $clave !!}</strong></p>
    </div>

@stop
{{--cerrar conntenido--}}