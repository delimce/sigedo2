@extends('layout.email')
@section('title', 'Nuevo Registro de '.$roll.' en iclasificados.com')
{{-- abrir el contenido--}}
@section('content')

    <div style="text-align: justify">
        Bienvenid@ {!! $nombre !!} ha sido creada una cuenta en el sitio, para gestión de sus avisos en línea.
        <p>Su nueva clave de acceso es: <strong>{!! $clave !!}</strong> pronto podrá visitarnos.</p>
    </div>

@stop
{{--cerrar conntenido--}}