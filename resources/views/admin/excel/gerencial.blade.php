<table id="grid1" class="table" style="text-align: center">
    <thead>
    <tr style="text-align: center">
        <th>Obra</th>
        <th>Estimados</th>
        <th>Total Docs</th>
        <th>A</th>
        <th>B</th>
        <th>C</th>
        <th>SE</th>
        <th>GT</th>
        <th>Bloq A</th>
        <th>Bloq otros</th>
        <th>CM</th>
        <th>ES</th>
        <th>PL</th>
        <th>MD</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>

    @foreach ($data as $item)
        <tr>
            <td style="text-align: left">{{utf8_encode($item->obra) }}</td>
            <td><?php $resumen->gerencial['testima'] += $item->est ?> {{ $item->est }}</td>
            <td><?php $resumen->gerencial['ttdosc'] += $item->tdocs ?>{{ $item->tdocs  }}</td>
            <td><?php $resumen->gerencial['testa'] += $item->estatus_a ?> {{$item->estatus_a}}</td>
            <td><?php $resumen->gerencial['testb'] += $item->estatus_b ?> {{$item->estatus_b }}</td>
            <td><?php $resumen->gerencial['testc'] += $item->estatus_c ?> {{$item->estatus_c }}</td>
            <td><?php $resumen->gerencial['testse'] += $item->estatus_se ?>{{$item->estatus_se}} </td>
            <td><?php $resumen->gerencial['tgt'] += $item->gt ?> {{$item->gt}}</td>
            <td><?php $resumen->gerencial['testba'] += $item->estatus_ba ?> {{$item->estatus_ba }}</td>
            <td><?php $resumen->gerencial['testblo'] += $item->estatus_blo ?> {{$item->estatus_blo}}</td>
            <td><?php $resumen->gerencial['tcm'] += $item->cm ?> {{$item->cm}}</td>
            <td><?php $resumen->gerencial['tes'] += $item->es ?> {{$item->es}}</td>
            <td><?php $resumen->gerencial['tpl'] += $item->pl ?> {{$item->pl}}</td>
            <td><?php $resumen->gerencial['tmd'] += $item->md ?> {{$item->md}}</td>
            <td><?php $resumen->gerencial['ttotal'] += $item->total ?> {{$item->total}}</td>
        </tr>
    @endforeach

    <tr style="font-weight: bold">
        <td>TOTALES</td>
        <td>{{$resumen->gerencial['testima']}}</td>
        <td>{{$resumen->gerencial['ttdosc']}}</td>
        <td>{{$resumen->gerencial['testa']}}</td>
        <td>{{$resumen->gerencial['testb']}}</td>
        <td>{{$resumen->gerencial['testc']}}</td>
        <td>{{$resumen->gerencial['testse']}}</td>
        <td>{{$resumen->gerencial['tgt']}}</td>
        <td>{{$resumen->gerencial['testba']}}</td>
        <td>{{$resumen->gerencial['testblo']}}</td>
        <td>{{$resumen->gerencial['tcm']}}</td>
        <td>{{$resumen->gerencial['tes']}}</td>
        <td>{{$resumen->gerencial['tpl']}}</td>
        <td>{{$resumen->gerencial['tmd']}}</td>
        <td>{{$resumen->gerencial['ttotal']}}</td>
        <?php
        $resumen->setGerencialTPGT($resumen->gerencial['tgt']);
        $resumen->setGerencialTAPY($resumen->gerencial['ttotal']);
        $resumen->setGerencialTGNRL($resumen->gerencial['ttdosc']);
        $resumen->setGerencialTPP($resumen->gerencial['testima'] - $resumen->gerencial['ttdosc']);

        ?>
    </tr>
    </tbody>
</table>


