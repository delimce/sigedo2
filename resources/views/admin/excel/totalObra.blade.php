<html>

@foreach ($data as $item)


    <table class="table">
        <thead>
        <tr>
            <th>Obra</th>
            <th>total Documentos</th>
            <th>Estatus A</th>
            <th>Estatus B</th>
            <th>Estatus C</th>
            <th>Estatus S/E</th>
            <th>Estatus N/A</th>
            <th>BLO</th>
        </tr>
        </thead>
        <tbody>

        <tr>
            <td>{{utf8_encode($item->obra)}}</td>
            <td>{{$resumen->getTotalStatusWhole($item)}}</td><?php $resumen->countDocs($resumen->getTotalStatusWhole($item)) ?>
            <td>{{$item->estatus_a}}</td><?php $resumen->countA($item->estatus_a) ?>
            <td>{{$item->estatus_b}}</td><?php $resumen->countB($item->estatus_b) ?>
            <td>{{$item->estatus_c}}</td><?php $resumen->countC($item->estatus_c) ?>
            <td>{{$item->estatus_se}}</td><?php $resumen->countSE($item->estatus_se) ?>
            <td>{{$item->estatus_na}}</td><?php $resumen->countNA($item->estatus_na) ?>
            <td>{{$item->estatus_blo}}</td><?php $resumen->countBlo($item->estatus_blo) ?>
        </tr>

        </tbody>
    </table>


@endforeach

<p>&nbsp;</p>

<table>
    <thead>
    <tr>
        <th>Documentos</th>
        <th>Estatus A</th>
        <th>Estatus B</th>
        <th>Estatus C</th>
        <th>Estatus S/E</th>
        <th>Estatus N/A</th>
        <th>BLO</th>
    </tr>
    </thead>
    <tbody>

    <tr>
        <td>{{$resumen->getTotalStatus("docs")}}</td>
        <td>{{$resumen->getTotalStatus("A")}}</td>
        <td>{{$resumen->getTotalStatus("B")}}</td>
        <td>{{$resumen->getTotalStatus("C")}}</td>
        <td>{{$resumen->getTotalStatus("SE")}}</td>
        <td>{{$resumen->getTotalStatus("NA")}}</td>
        <td>{{$resumen->getTotalStatus("blo")}}</td>
    </tr>

    </tbody>
</table>



</html>