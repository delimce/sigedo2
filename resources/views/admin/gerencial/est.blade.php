@extends('layout.main')
@section('title', 'Gerencial - Documentos Estimados')
@section('content')
    <div class="col-lg-12">
        <form name="form1" id="form1" role="form" data-toggle="validator">

            <div class="col-lg-12">
                <ul class="list-group">

                    @foreach ($data as $result)
                        <li class="list-group-item">
                        <span>
                         {{$result->CodSubObr}} &nbsp; {{$result->DesSubObr}}
                        </span>

                            <span  style="float: right">
                                <input name="est_{{$result->CodSubObr}}" type="number" value="{{$result->EstDocs}}" class="input-small" placeholder="total">
                            </span>

                        </li>
                    @endforeach

                </ul>

                <div style="padding-bottom: 16px; text-align: right">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <button id="save" type="submit" class="btn btn-primary">Guardar</button>
                    <button type="reset" class="btn btn-default">Borrar</button>
                </div>
            </div>


        </form>
    </div>
@endsection


@section('foot')
    {!! Html::script('assets/js/sigedo2.js') !!}

    <script>
        $("#form1").submit(function (e) {
            saveEst();
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

    </script>

@stop