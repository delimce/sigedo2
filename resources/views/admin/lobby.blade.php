@extends('layout.main')
@section('title', 'Inicio')
@section('content')


    {{--lista de contratos--}}

    <div class="col-lg-10">
        <div class="panel panel-info">
            <div class="panel-heading">
                Listado de Contratos
            </div>
            <div class="panel-body">

                <div class="table-responsive table-bordered">
                    <table class="table">
                        <thead>
                        <tr>
                            <th width="20%" align="center">Código</th>
                            <th width="*">Titulo</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($obras as $item)

                            <tr>
                                <td align="center">{{$item->CodObr }}</td>
                                <td>{{$item->Descri}}</td>
                            </tr>

                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div>


    {{--ultimos documentos--}}


    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Documentos Recientes
            </div>
            <div class="panel-body">

                <div class="table-responsive table-bordered">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Contrato</th>
                            <th>Código</th>
                            <th>Tipo</th>
                            <th>Titulo</th>
                            <th>Fecha</th>
                            <th>Estatus</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($ultimos as $item)

                            <tr>
                                <td>{{$item->CodDoc }}</td>
                                <td>{{$item->CodObr }}</td>
                                <td>{{$item->getCodigo($item)}}</td>
                                <td>{{$item->tipo->DesTipDoc}}</td>
                                <td>{{$item->TitDoc}}</td>
                                <td>{{$item->getUltMov()->FecDoc}}</td>
                                <td>{{$item->getUltMov()->EstDoc}}</td>
                            </tr>

                        @endforeach

                        </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div>





@endsection