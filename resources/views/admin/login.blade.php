@extends('layout.login')
@section('title', 'Login')
@section('head')

@stop
{{-- abrir el contenido--}}
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        {!! HTML::image('assets/images/contuy-logo.gif') !!}
                    </div>
                    <div class="panel-body">
                        <form name="form1" id="form1" role="form">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control success" placeholder="E-mail" name="email" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Recordarme
                                    </label>
                                </div>
                                <div id="message"></div>
                                <!-- Change this to a button or input when using this as a form -->
                                <a href="javascript:userLogin()" class="btn btn-lg btn-success btn-block">Entrar</a>
                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
{{--cerrar conntenido--}}
@section('foot')
    <script src="{{url("./assets/js/user.js")}}"></script>
@stop