<div class="panel-body">
    <div class="row">

        <div class="container">

            <form name="form1" method="GET" id="form1" role="form"
                  data-toggle="validator" novalidate="true">




                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-grup">
                            <label>Fecha Desde</label>
                            {!! Form::date('fecha1', $fecha1, array('class' => 'form-control','id' => 'fecha1')) !!}
                        </div>
                    </div>

                    <div class="col-sm-5">
                        <div class="form-grup">
                            <label>Fecha Hasta</label>
                            {!! Form::date('fecha2', $fecha2, array('class' => 'form-control','id' => 'fecha2')) !!}
                        </div>
                    </div>
                </div>


                {!! Form::token() !!}
                <!-- /.tabbable -->

                <div class="row">
                    <div class="col-xs-11">
                        <br>
                        <button id="search" type="submit" class="btn btn-primary">
                            Buscar
                        </button>
                    </div>

                </div>
            </form>


        </div>
    </div>

</div>