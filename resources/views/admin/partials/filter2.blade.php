<div class="hidden-print">
    <div class="panel-body">
        <div class="row">

            <div class="container">

                <form name="form1" method="GET" id="form1" role="form"
                      data-toggle="validator" novalidate="true">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-grup">
                                <label>Fase de Proyecto</label>
                                {!! Form::select('fase',$fase,$faseValue, array('class' => 'form-control','id'=>'fase'))  !!}
                            </div>
                        </div>


                        <div class="col-sm-5">
                            <div class="form-grup">
                                <label>Obra</label>
                                {!! Form::select('obra',$obra,$obraValue, array('class' => 'form-control','id'=>'obra'))  !!}
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-5">
                            <div class="form-grup">
                                <label>Tipo de Documento</label>
                                {!! Form::select('tipo',$tipo,$tipoValue, array('class' => 'form-control','id'=>'tipo'))  !!}
                            </div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-grup">
                                <label>Ente</label>
                                {!! Form::select('persona',$persona,$personaValue, array('class' => 'form-control','id'=>'persona'))  !!}
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-grup">
                                <label>Area</label>
                                {!! Form::select('area',$area,$areaValue, array('class' => 'form-control','id'=>'area'))  !!}
                            </div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-grup">
                                <label>Sub Area</label>
                                {!! Form::select('subarea',$subarea,$subareaValue, array('class' => 'form-control','id'=>'subarea'))  !!}
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-grup">
                                <label>Tramo</label>
                                {!! Form::select('tramo',$tramo,$tramoValue, array('class' => 'form-control','id'=>'tramo'))  !!}
                            </div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-grup">
                                <label>Unidad Constructiva</label>
                                {!! Form::select('unidad',$unid,$unidValue, array('class' => 'form-control','id'=>'unid'))  !!}
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-grup">
                                <label>Fecha Desde</label>
                                {!! Form::date('fecha1', $fecha1, array('class' => 'form-control','id' => 'fecha1')) !!}
                            </div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-grup">
                                <label>Fecha Hasta</label>
                                {!! Form::date('fecha2', $fecha2, array('class' => 'form-control','id' => 'fecha2')) !!}
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-grup">
                                <label>Cod de Carta</label>
                                {!! Form::text('carta', $carta, array('class' => 'form-control','id' => 'carta')) !!}
                            </div>
                        </div>

                        <div class="col-sm-5">
                            <div class="form-grup">
                                <label>Estatus</label>
                                {!! Form::select('estatus',$estatus,$estatusValue, array('class' => 'form-control','id'=>'estatus'))  !!}
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-grup">
                                <label>Condición</label>
                                {!! Form::select('condicion',$condicion,$condValue, array('class' => 'form-control','id' => 'condicion')) !!}
                            </div>
                        </div>

                    </div>


                    {!! Form::token() !!}
                            <!-- /.tabbable -->

                    <div class="row">
                        <div class="col-xs-11">
                            <br>
                            <button id="search" type="button" class="btn btn-primary">
                                Buscar
                            </button>
                            &nbsp;&nbsp;
                            <button id="2excel" type="button" class="btn btn-success">
                                Exportar a Excel
                            </button>
                        </div>

                    </div>
                </form>


            </div>
        </div>

    </div>

</div>