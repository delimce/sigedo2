<div style="padding-left: 15px" class="hidden-print">
    <form name="form1" method="GET" id="form1" role="form" novalidate="true">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-grup">
                    <label>Fecha Desde</label>
                    {!! Form::date('fecha1', $fecha1, array('class' => 'form-control','id' => 'fecha1')) !!}
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-grup">
                    <label>Fecha Hasta</label>
                    {!! Form::date('fecha2', $fecha2, array('class' => 'form-control','id' => 'fecha2')) !!}
                </div>
            </div>
        </div>

        {!! Form::token() !!}
    </form>
    <p>&nbsp;</p>
    <div class="row">
        <div class="col-xs-11">
            <button id="search" type="button" class="btn btn-primary">
                Buscar
            </button>


            @if(count($data)>0)
                &nbsp;&nbsp;
                <button id="2excel" type="button" class="btn btn-success">
                    Exportar a Excel
                </button>
                &nbsp;&nbsp;
                <button id="2print" type="button" class="btn btn-warning">
                    Imprimir
                </button>
            @endif

        </div>
    </div>
</div>
<br>