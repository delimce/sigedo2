<li class="sidebar-search">
    <div class="input-group custom-search-form">
<div>Contrato actual:</div>
        {!! Form::select('obra_cod',Session::get('OBRAS'),Session::get('OBRASELECT'), array('class' => 'form-control','id'=>'obra_cod','onchange'=>"selectObra(this.value)"))  !!}


    </div>
    <!-- /input-group -->
</li>
<li>
    <a href="{{url("admin")}}"><i class="fa fa-home"></i> Inicio</a>
</li>


@if (in_array(3,Session::get('DATAUSER')->accesos))
    <li>
        <a href=""><i class="fa fa-line-chart"></i> Reportes<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li>
                <a href="{{url('admin/reports/general')}}"><i class="fa fa-check-circle-o"></i> General</a>
            </li>
            <li>
                <a href="{{url('admin/reports/totalObra')}}"><i class="fa fa-check-circle-o"></i> Total por Obra</a>
            </li>
            <li>
                <a href="{{url('admin/reports/docsObra')}}"><i class="fa fa-check-circle-o"></i> Docs. por Obra</a>
            </li>
            <li>
                <a href="{{url('admin/reports/envRecib')}}"><i class="fa fa-check-circle-o"></i> Enviado y Recibido</a>
            </li>
            <li>
                <a href="{{url('admin/reports/docsAB')}}"><i class="fa fa-check-circle-o"></i> Docs A & B</a>
            </li>
        </ul>
        <!-- /.nav-second-level -->
    </li>
@endif

@if (in_array(4,Session::get('DATAUSER')->accesos))
    <li>
        <a href=""><i class="fa fa-industry"></i></i> Gerencial<span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li>
                <a href="{{url('admin/gerencial/est')}}"><i class="fa fa-check-circle-o"></i> Valores Est.</a>
            </li>
            <li>
                <a href="{{url('admin/gerencial/report')}}"><i class="fa fa-check-circle-o"></i> Reporte</a>
            </li>

        </ul>
    </li>
@endif


@if (in_array(2,Session::get('DATAUSER')->accesos))
    <li>
        <a href="{{url("admin/profile/list")}}"><i class="fa fa-users"></i></i> Perfiles</a>
    </li>
@endif

@if (in_array(1,Session::get('DATAUSER')->accesos))
    <li>
        <a href="{{url("admin/user/list")}}"><i class="fa fa-user"></i> Usuarios</a>
    </li>
@endif

<div class="logodeve">
    {!! HTML::image('assets/images/logodeve.png') !!}
</div>
