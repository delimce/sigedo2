<li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
      <span id="userfullname"> {{Session::get('DATAUSER')->nombre.' '.Session::get('DATAUSER')->apellido}}</span> &nbsp;
        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-user">
        <li><a href="{{url("admin/myAccount")}}"><i class="fa fa-user fa-fw"></i>Mis Datos</a>
        </li>
        <li><a href="{{url("admin/password")}}"><i class="fa fa-key"></i> Cambiar Clave</a>
        </li>
        <li class="divider"></li>
        <li><a href="{{url("admin/logout")}}"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
        </li>
    </ul>
    <!-- /.dropdown-user -->
</li>