<div class="col-lg-10">
    <div class="panel panel-green">
        <div class="panel-heading">
            Totales por Estatus
        </div>
        <div class="panel-body">

            <div class="table-responsive table-bordered">
                <table class="table">
                    <thead>
                    <tr>
                        <th>N° Documentos</th>
                        <th>Estatus A</th>
                        <th>Estatus B</th>
                        <th>Estatus C</th>
                        <th>Estatus S/E</th>
                        <th>Estatus N/A</th>
                        <th>BLO</th>
                    </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td>{{$resumen->getTotalDocs()}}</td>
                            <td>{{$resumen->getTotalStatus("A")}}</td>
                            <td>{{$resumen->getTotalStatus("B")}}</td>
                            <td>{{$resumen->getTotalStatus("C")}}</td>
                            <td>{{$resumen->getTotalStatus("SE")}}</td>
                            <td>{{$resumen->getTotalStatus("NA")}}</td>
                            <td>{{$resumen->getTotalStatus("blo")}}</td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>

    </div>
</div>


