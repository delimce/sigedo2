<div class="col-lg-5">
    <div class="panel panel-green">
        <div class="panel-heading">
            Totales
        </div>
        <div class="panel-body">

            <div class="table-responsive table-bordered">
                <table class="table" style="text-align: left">

                    <tr>
                        <td>Total documentos producidos a Rev GT</td>
                        <td>{{$resumen->getGerencialTPGT()}}</td>

                    </tr>

                    <tr>
                        <td>Total Documentos de Apoyo (Internos)</td>
                        <td>{{$resumen->getGerencialTAPY()}}</td>

                    </tr>

                    <tr>
                        <td>Total General de documentos producidos</td>
                        <td>{{$resumen->getGerencialTGNRL()}}</td>

                    </tr>

                    <tr>
                        <td>Total Documentos por Producir</td>
                        <td>{{$resumen->getGerencialTPP()}}</td>

                    </tr>

                </table>
            </div>

        </div>

    </div>
</div>


