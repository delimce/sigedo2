@extends('layout.main')
@section('title', 'Reportes - Documentos en A y B')
@section('head')


@stop
@section('content')

    <br>
    @include('admin.partials.filter3')
    <p>&nbsp;</p>


    @if(count($data)>0)

        <div class="col-lg-12">
            <div class="panel panel-green">
                <div class="panel-heading">
                    Documentos en A y B por Obras
                </div>
                <div class="panel-body">

                    <div class="table-responsive table-bordered">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Obra Ferroviaria</th>
                                <th>Total</th>
                                <th>PL</th>
                                <th>CR</th>
                                <th>MD</th>
                                <th>CM</th>
                                <th>CA</th>
                                <th>ES</th>
                                <th>INF</th>
                                <th>ESQ</th>
                                <th>Otros</th>
                                <th>Descripción</th>
                                <th>Total A</th>
                                <th>Total B</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach ($data as $item)

                                <tr>
                                    <td>{{utf8_encode($item->obra)}}</td>
                                    <td><?php
                                        print $docs1 = $item->planos + $item->croquis + $item->memorias +
                                                $item->computos + $item->calculos + $item->especifica
                                                + $item->informes + $item->esquematicos + $item->otros;
                                        $resumen->countDocs($docs1);

                                        ?></td>
                                    <td>{{$item->planos}} <?php $resumen->setTotalTipo("pl", $item->planos) ?></td>
                                    <td>{{$item->croquis}} <?php $resumen->setTotalTipo("cr", $item->croquis) ?></td>
                                    <td>{{$item->memorias}} <?php $resumen->setTotalTipo("md", $item->memorias) ?></td>
                                    <td>{{$item->computos}} <?php $resumen->setTotalTipo("cm", $item->computos) ?></td>
                                    <td>{{$item->calculos}} <?php $resumen->setTotalTipo("ca", $item->calculos) ?></td>
                                    <td>{{$item->especifica}} <?php $resumen->setTotalTipo("es", $item->especifica) ?></td>

                                    <td>{{$item->informes}} <?php $resumen->setTotalTipo("inf", $item->informes) ?></td>
                                    <td>{{$item->esquematicos}} <?php $resumen->setTotalTipo("esq", $item->esquematicos) ?></td>
                                    <td>{{$item->otros}} <?php $resumen->setTotalTipo("otros", $item->otros) ?></td>

                                    <?php

                                    $obra->setSubObra($item->CodSubObr);
                                    $obra->setFecha1($fecha1);
                                    $obra->setFecha2($fecha2);

                                    $detalle = $obra->getDocsABDetail();


                                    ?>

                                    <td>
                                        @foreach ($detalle as $detail)
                                            <div>{{$detail->descrip}}</div>
                                        @endforeach
                                    </td>

                                    <td>
                                        @foreach ($detalle as $detail)
                                            <div>{{$detail->a}}</div>
                                        @endforeach
                                    </td>

                                    <td>
                                        @foreach ($detalle as $detail)
                                            <div>{{$detail->b}}</div>
                                        @endforeach
                                    </td>

                                </tr>

                            @endforeach


                            <tr style="font-weight: bolder">
                                <td>Totales:</td>
                                <td><?=$resumen->getTotalStatus("docs")?></td>
                                <td><?=$resumen->getTotalTipo("pl")?></td>
                                <td><?=$resumen->getTotalTipo("cr")?></td>
                                <td><?=$resumen->getTotalTipo("md")?></td>
                                <td><?=$resumen->getTotalTipo("cm")?></td>
                                <td><?=$resumen->getTotalTipo("ca")?></td>
                                <td><?=$resumen->getTotalTipo("es")?></td>
                                <td><?=$resumen->getTotalTipo("inf")?></td>
                                <td><?=$resumen->getTotalTipo("esq")?></td>
                                <td><?=$resumen->getTotalTipo("otros")?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>


                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>


    @endif



@endsection


@section('foot')


    <script>

        $('#search').click(function () {
            $('#form1').attr('action', '{{url("admin/reports/docsAB")}}');
            $("#form1").submit();
        });

        $('#2excel').click(function () {
            $('#form1').attr('action', '{{url("admin/reports/docsAB/excel")}}');
            $("#form1").submit();
        });

        $('#2print').click(function () {
            window.print();
        });

    </script>


@stop