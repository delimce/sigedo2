@extends('layout.main')
@section('title', 'Reportes - Documentos Por Obra')
@section('head')


@stop
@section('content')

    <br>
    @include('admin.partials.filter3')
    <p>&nbsp;</p>



    <?php $obra = -1; ?>

    @foreach ($data as $item)


        <?php
        if ($obra != $item->CodSubObr) {

            if ($obra == -1) {
                abre($item->obra);
            } else {
                resumenObra($resumen);
                cierra();
                abre($item->obra);
                $resumen->resetDocs();
            }

            $obra = $item->CodSubObr;

        }



        ?>


        <tr>
            <td>{{$item->tipo}}</td>
            <td>{{$resumen->getTotalStatusWhole($item)}}<?php $resumen->countDocs($resumen->getTotalStatusWhole($item)) ?></td>
            <td>{{$item->estatus_a}}<?php $resumen->countA($item->estatus_a) ?></td>
            <td>{{$item->estatus_b}}<?php $resumen->countB($item->estatus_b) ?></td>
            <td>{{$item->estatus_c}}<?php $resumen->countC($item->estatus_c) ?></td>
            <td>{{$item->estatus_se}}<?php $resumen->countSE($item->estatus_se) ?></td>
            <td>{{$item->estatus_na}}<?php $resumen->countNA($item->estatus_na) ?></td>
            <td>{{$item->estatus_blo}}<?php $resumen->countBlo($item->estatus_blo) ?></td>
        </tr>



    @endforeach


    <?php

    if (count($data) > 0) {
        resumenObra($resumen);
        cierra();
    }


    ?>

@endsection


@section('foot')


    <script>

        $('#search').click(function () {
            $('#form1').attr('action', '{{url("admin/reports/docsObra")}}');
            $("#form1").submit();
        });

        $('#2excel').click(function () {
            $('#form1').attr('action', '{{url("admin/reports/docsObra/excel")}}');
            $("#form1").submit();
        });

        $('#2print').click(function () {
            window.print();
        });

    </script>



@stop


<?php

function abre($titulo)
{

?>
<div class="col-lg-10">
    <div class="panel panel-green">
        <div class="panel-heading">
            Totales por {{utf8_encode($titulo)}}
        </div>
        <div class="panel-body">

            <div class="table-responsive table-bordered">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Tipo</th>
                        <th>N° Docs</th>
                        <th>Estatus A</th>
                        <th>Estatus B</th>
                        <th>Estatus C</th>
                        <th>Estatus S/E</th>
                        <th>Estatus N/A</th>
                        <th>BLO</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    }

                    ?>

                    <?php

                    function cierra()
                    {

                    ?>
                    </tbody>
                </table>
            </div>

        </div>

    </div>
</div>
<?php

}


function resumenObra($resumen1){
?>
<tr style="font-weight: bold">
    <td>TOTALES:</td>
    <td>{{$resumen1->getTotalStatus("docs")}}</td>
    <td>{{$resumen1->getTotalStatus("A")}}</td>
    <td>{{$resumen1->getTotalStatus("B")}}</td>
    <td>{{$resumen1->getTotalStatus("C")}}</td>
    <td>{{$resumen1->getTotalStatus("SE")}}</td>
    <td>{{$resumen1->getTotalStatus("NA")}}</td>
    <td>{{$resumen1->getTotalStatus("blo")}}</td>
</tr>
<?php
}

?>