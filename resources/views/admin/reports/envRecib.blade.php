@extends('layout.main')
@section('title', 'Reportes - Enviado y Recibido')
@section('head')
        <!-- Bootstrap datatable -->
{!! Html::style('assets/libs/datatables/media/css/jquery.dataTables.min.css') !!}
{!! Html::style('https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css') !!}

@stop
@section('content')

    <div id="filtro">
        @include('admin.partials.filter2')
    </div>


    <div id="docs">
        @if(!empty($docs))

            <div class="table-responsive">
                <table id="grid1" class="table">
                    <thead>
                    <tr>
                        <th>Contrato</th>
                        <th>Código</th>
                        <th>Fecha</th>
                        <th>Carta</th>
                        <th>Titulo</th>
                        <th>Rev act.</th>
                        <th>Estatus act.</th>
                        <th>Rev ant.</th>
                        <th>Estatus ant.</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($docs as $item)
                        <td>{{$item->CodObr }}</td>
                        <td>{!! HTML::link('admin/reports/movs/'.$item->CodDoc, $item->getCodigo($item)) !!} </td>
                        <?php $item->getUltMov(); ?>
                        <td>{{$item->getMovs("date")}}</td>
                        <td>{{$item->getMovs("carta")}}</td>
                        <td>{{utf8_encode($item->TitDoc)}}</td>
                        <td>{{$item->getMovs("rev")}}</td>
                        <td>{{$item->getMovs("status")}}</td>
                        <?php $item->getPenUltMov(); ?>
                        <td>{{$item->getSubmovs("rev")}}</td>
                        <td>{{$item->getSubmovs("status")}}</td>
                        </tr>
                    @endforeach
                    <p></p>
                    </tbody>
                </table>
            </div>



        @endif

    </div>



@endsection


@section('foot')


    <script>

        $('#search').click(function () {
            $('#form1').attr('action', '{{url("admin/reports/envRecib")}}');
            $("#form1").submit();
        });

        $('#2excel').click(function () {
            $('#form1').attr('action', '{{url("admin/reports/envrec/excel")}}');
            $("#form1").submit();
        });

    </script>

    <!-- datatable-->
    <script src="{{url("https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js")}}"></script>
    <script src="{{url("https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js")}}"></script>
    <script src="{{url("https://cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js")}}"></script>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            $('#grid1').DataTable({
                iDisplayLength: 25,
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'print',
                        text: 'Imprimir Documentos'
                    }
                ]

            });





        });

    </script>

@stop