@extends('layout.main')
@section('title', "Detalles del Documento")
@section('head')
        <!-- Bootstrap datatable -->
{!! Html::style('assets/libs/datatables/media/css/jquery.dataTables.min.css') !!}

@stop
@section('content')

    <h3>Documento:</h3>
    <div>{{$doc->getCodigo($doc)}}</div>
    <div>{{$doc->ParObrDoc}}</div>
    <div>{{$doc->TitDoc}}</div>
    <div>{{$doc->ObjObr}}</div>

    <h3>Movimientos:</h3>
    <div id="movs">
        <div class="table-responsive">
            <table id="grid1" class="table">
                <thead>
                <tr>
                    <th>Correlativo</th>
                    <th>Carta</th>
                    <th>Ubicación</th>
                    <th>Fecha</th>
                    <th>Condición</th>
                    <th>Revisión</th>
                    <th>Estatus</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($movs as $item)
                    <td>{{$item->MovCor}} </td>
                    <td>{{$item->CorOri}} </td>
                    <td>{{$personas[$item->CodMov]}}</td>
                    <td>{{$item->FecDoc}}</td>
                    <td>{{$item->TipMov}}</td>
                    <td>{{$item->NumRev}}</td>
                    <td>{{$item->EstDoc}}</td>
                    </tr>
                @endforeach
                <p></p>
                </tbody>
            </table>
        </div>
    </div>


    <p>
        <button id="back" onclick="history.back()" type="button" class="btn btn-default">Regresar</button>
        &nbsp;
        <button id="2excel" type="button" class="btn btn-success">
            Exportar a Excel
        </button>
    </p>

    @endsection


    @section('foot')

            <script>

                $('#2excel').click(function () {

                    location.href = '{{url("admin/reports/movs/excel/".$doc->CodDoc)}}';

                });

            </script>

            <!-- datatable-->
    <script src="{{url("assets/libs/datatables/media/js/jquery.dataTables.min.js")}}"></script>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            $('#grid1').DataTable();
        });
    </script>

@stop