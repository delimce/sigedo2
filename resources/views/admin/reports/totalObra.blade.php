@extends('layout.main')
@section('title', 'Reportes - Total Por Obra')
@section('head')


@stop
@section('content')
    <br>
    @include('admin.partials.filter3')
    <p>&nbsp;</p>

    @foreach ($data as $item)


        <div class="col-lg-10">
            <div class="panel panel-green">
                <div class="panel-heading">
                    Totales por {{utf8_encode($item->obra)}}

                </div>
                <div class="panel-body">

                    <div class="table-responsive table-bordered">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>N° Documentos</th>
                                <th>Estatus A</th>
                                <th>Estatus B</th>
                                <th>Estatus C</th>
                                <th>Estatus S/E</th>
                                <th>Estatus N/A</th>
                                <th>BLO</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td>{{$resumen->getTotalStatusWhole($item)}}</td><?php $resumen->countDocs($resumen->getTotalStatusWhole($item)) ?>
                                <td>{{$item->estatus_a}}</td><?php $resumen->countA($item->estatus_a) ?>
                                <td>{{$item->estatus_b}}</td><?php $resumen->countB($item->estatus_b) ?>
                                <td>{{$item->estatus_c}}</td><?php $resumen->countC($item->estatus_c) ?>
                                <td>{{$item->estatus_se}}</td><?php $resumen->countSE($item->estatus_se) ?>
                                <td>{{$item->estatus_na}}</td><?php $resumen->countNA($item->estatus_na) ?>
                                <td>{{$item->estatus_blo}}</td><?php $resumen->countBlo($item->estatus_blo) ?>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
        </div>



    @endforeach


    <p>&nbsp;</p>


    @if(count($data)>0)

    <div class="col-lg-10">
        <div class="panel panel-red">
            <div class="panel-heading">
               TOTALES
            </div>
            <div class="panel-body">

                <div class="table-responsive table-bordered">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>N° Documentos</th>
                            <th>Estatus A</th>
                            <th>Estatus B</th>
                            <th>Estatus C</th>
                            <th>Estatus S/E</th>
                            <th>Estatus N/A</th>
                            <th>BLO</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>{{$resumen->getTotalStatus("docs")}}</td>
                            <td>{{$resumen->getTotalStatus("A")}}</td>
                            <td>{{$resumen->getTotalStatus("B")}}</td>
                            <td>{{$resumen->getTotalStatus("C")}}</td>
                            <td>{{$resumen->getTotalStatus("SE")}}</td>
                            <td>{{$resumen->getTotalStatus("NA")}}</td>
                            <td>{{$resumen->getTotalStatus("blo")}}</td>
                        </tr>

                        </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div>

    @endif



@endsection


@section('foot')


    <script>

        $('#search').click(function () {
            $('#form1').attr('action', '{{url("admin/reports/totalObra")}}');
            $("#form1").submit();
        });

        $('#2excel').click(function () {
            $('#form1').attr('action', '{{url("admin/reports/totalObra/excel")}}');
            $("#form1").submit();
        });

        $('#2print').click(function () {
            window.print();
        });

    </script>



@stop