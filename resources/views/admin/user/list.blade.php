@extends('layout.main')
@section('title', 'Usuarios - Lista')
@section('head')
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <!-- Bootstrap datatable -->
    {!! Html::style('assets/libs/datatables/media/css/jquery.dataTables.min.css') !!}

@stop
@section('content')

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Lista de Usuarios
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="grid1" class="table">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Perfil</th>
                            <th>estatus</th>
                            <th>Creado</th>
                            <th>
                                &nbsp;
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($data as $result)
                            <tr>
                                <td>{{ $result->id }}</td>
                                <td>{{ $result->nombre }} {{ $result->apellido }}</td>
                                <td>{{ $result->email }}</td>
                                <td>{{ $result->profile->descripcion }}</td>
                                <td>{{ $result->estatus }}</td>
                                <td>{{ date(getenv('DATE_FORMAT'), strtotime($result->created_at)) }}</td>

                                <td>
                                    <a data-toggle="modal" onclick="goModule({{ $result->id }})" data-target="#modulex"
                                       title="editar"> <i class="fa fa-pencil-square-o"></i> </a>
                                    &nbsp;
                                    <a onclick="deleteUser({{ $result->id }})" title="borrar"> <i
                                                class="fa fa-trash-o"></i></a>
                                    &nbsp;
                                    <a href="{{url("admin/user/password/$result->id")}}" title="cambiar clave">
                                        <i class="fa fa-key"></i> </a>
                                </td>
                            </tr>
                        @endforeach
                        <p></p>
                        </tbody>
                    </table>
                </div>
                <a class="btn btn-primary" data-toggle="modal" onclick="goModule(0)"
                   data-target="#modulex"> Nuevo Usuario</a>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>




    <div id="modulex" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Content will be loaded here from file -->
                <div class="panel panel-default">
                    <div id="hname" class="panel-heading">
                        Crear / Editar Usuario
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <form name="form1" id="form1" role="form" data-toggle="validator">

                            <div class="form-group">
                                <label class="required">Nombre</label>
                                <input name="nombre" id="nombre" class="form-control" placeholder="escriba un nombre"
                                       required>

                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <label class="required">Apellido</label>
                                <input name="apellido" id="apellido" class="form-control"
                                       placeholder="escriba un apellido"
                                       required>

                                <div class="help-block with-errors"></div>
                            </div>


                            <div class="form-group">
                                <label class="required">Identificación</label>
                                <input name="identificacion" id="identificacion" class="form-control"
                                       placeholder="escriba su identificación"
                                       required>

                                <div class="help-block with-errors"></div>
                            </div>


                            <div class="form-group">
                                <label class="required">Perfil</label>
                                {!! Form::select('perfil_id',$perfiles,null, array('class' => 'form-control','id'=>'perfil_id'))  !!}
                            </div>


                            <div class="form-group">
                                <label class="required">Email</label>
                                <input name="email" id="email" type="email" class="form-control"
                                       placeholder="escriba un email" required>

                                <div class="help-block with-errors"></div>
                            </div>


                            <div id="passblock">
                                <div class="form-group">
                                    <label class="required">Password (min 6 caracteres)</label>
                                    <input name="password" id="password" type="password" data-minlength="6"
                                           class="form-control" placeholder="escriba un password" required>

                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group">
                                    <label class="required">Confirmar Password</label>
                                    <input name="pass2" id="pass2" data-minlength="6" type="password"
                                           class="form-control"
                                           placeholder="repita la clave"
                                           data-match="#password" data-match-error="Las claves no coinciden" required>

                                    <div class="help-block with-errors"></div>
                                </div>

                            </div>


                            <div class="form-group">
                                <label class="required">Telefono1</label>
                                <input name="tlf1" id="tlf1" class="form-control" placeholder="escriba un telefono"
                                       required>

                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <label class="required">Estatus</label>
                                {!! Form::select('estatus',$estatus,null, array('class' => 'form-control','id'=>'estatus'))  !!}
                            </div>


                            <button id="save" type="submit" class="btn btn-primary">
                                Guardar
                            </button>
                        </form>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
    </div>


@endsection




@section('foot')


    <script type="text/javascript">

        $(document).ready(function () {

            $("#form1").submit(function (e) {
                saveOrUpdate();
                e.preventDefault(); // avoid to execute the actual submit of the form.
            });

        });

    </script>

    <!-- Jquery plugins-->
    <script src="{{url("assets/js/utils.js")}}"></script>
    <script src="{{url("assets/libs/jquery-plugins/jquery.confirm.min.js")}}"></script>



    <!-- datatable-->
    <script src="{{url("assets/libs/datatables/media/js/jquery.dataTables.min.js")}}"></script>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $('#grid1').DataTable();
        } );
    </script>
    <script src="{{url("assets/js/user.js")}}"></script>

@stop
