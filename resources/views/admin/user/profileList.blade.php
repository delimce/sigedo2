@extends('layout.main')
@section('title', 'Perfiles de usuario - Lista')
@section('head')
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <!-- Bootstrap datatable -->
    {!! Html::style('assets/libs/datatables/media/css/jquery.dataTables.min.css') !!}
@stop
@section('content')

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Lista de perfiles
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="grid1" class="table">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Perfil</th>
                            <th>Módulos</th>
                            <th>Creado</th>
                            <th>
                                &nbsp;
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($data as $result)
                            <tr>
                                <td>{{ $result->id }}</td>
                                <td>{{ $result->descripcion }}</td>
                                <td>{{ $result->modulos }}</td>
                                <td>{{ date(getenv('DATE_FORMAT'), strtotime($result->created_at)) }}</td>
                                <td>
                                    <a data-toggle="modal" onclick="goModule({{ $result->id }})" data-target="#modulex"
                                       title="editar"> <i class="fa fa-pencil-square-o"></i> </a>
                                    &nbsp;
                                    <a onclick="deletePro({{ $result->id }})" title="borrar"> <i
                                                class="fa fa-trash-o"></i></a>

                                </td>
                            </tr>
                        @endforeach
                        <p></p>
                        </tbody>
                    </table>
                </div>
                <a class="btn btn-primary" data-toggle="modal" onclick="goModule(0)"
                   data-target="#modulex"> Nuevo Perfil</a>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>




    <div id="modulex" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Content will be loaded here from file -->
                <div class="panel panel-default">
                    <div id="hname" class="panel-heading">
                        Crear / Editar Perfil de usuario
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <form name="form1" id="form1" role="form" data-toggle="validator">

                            <div class="form-group">
                                <label class="required">Nombre</label>
                                <input name="nombre" id="nombre" class="form-control" placeholder="escriba un nombre"
                                       required>

                                <div class="help-block with-errors"></div>
                            </div>

                            @foreach ($modulos as $modulo)
                                <div class="form-group">

                                    {!! Form::checkbox('modulos[]', $modulo->id, false, ['class' => 'field']) !!}
                                    <span> {{$modulo->nombre}}</span>
                                </div>
                            @endforeach

                            <button id="save" type="submit" class="btn btn-primary">
                                Guardar
                            </button>
                        </form>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
        </div>
    </div>


@endsection



@section('foot')


    <script type="text/javascript">

        $(document).ready(function () {

            $("#form1").submit(function (e) {
                saveOrUpdate();
                e.preventDefault(); // avoid to execute the actual submit of the form.
            });

        });

    </script>

    <!-- Jquery plugins-->
    <script src="{{url("assets/js/utils.js")}}"></script>
    <script src="{{url("assets/libs/jquery-plugins/jquery.confirm.min.js")}}"></script>


    <!-- datatable-->
    <script src="{{url("assets/libs/datatables/media/js/jquery.dataTables.min.js")}}"></script>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            $('#grid1').DataTable();
        });
    </script>
    <script src="{{url("assets/js/profile.js")}}"></script>

@stop
