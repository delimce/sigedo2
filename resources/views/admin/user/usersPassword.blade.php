@extends('layout.main')
@section('title', 'Usuarios - Cambio de clave')
@section('head')
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
@stop
@section('content')
    <div class="col-lg-8">
        <form name="form2" id="form2" role="form" data-toggle="validator">

            <div class="form-group">
                <label>Nombre</label>
                <input name="nombre2" id="nombre2" value="{{$data->nombre}} {{$data->apellido}}" class="form-control"
                       placeholder="escriba un nombre" readonly="yes">
            </div>

            <div class="form-group">
                <label>Email</label>
                <input name="email2" id="email2" value="{{$data->email}}" class="form-control" readonly="yes" required>
            </div>

            <div class="form-group">
                <label>Ingrese la nueva clave (Mayor a 5 caracteres)</label>
                <input name="pass1" id="pass1" data-minlength="6"
                       type="password" class="form-control" placeholder="coloque una clave" required>
            </div>

            <div class="form-group">
                <input name="pass2" id="pass2" data-minlength="6" type="password"
                       class="form-control" placeholder="repita la clave"
                       data-match="#pass1" data-match-error="Las claves no coinciden" required>

                <div class="help-block with-errors"></div>
            </div>
            {!! Form::hidden('id2',$data->id,array('class' => 'form-control','id'=>'id2')) !!}
            <button id="save2" type="submit" class="btn btn-primary">Cambiar</button>
            <button id="back" onclick="history.back()" type="button" class="btn btn-default">Regresar</button>

        </form>

    </div>

@endsection


@section('foot')

    {!! Html::script('assets/js/utils.js') !!}
    {!! Html::script('assets/js/user.js') !!}

    <script>
        $("#form2").submit(function(e) {
            changePassword2();
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

    </script>

@stop