@extends('layout.login')
@section('title', 'Error Inesperado')
@section('head')

@stop
{{-- abrir el contenido--}}
@section('content')

    <p>&nbsp;</p>
    <div style="text-align: center">
        <div style="font-size: 40px;">
            <i class="fa fa-exclamation-triangle"></i>
        </div>
        <div style="font-size: 20px; padding: 23px">

            El sistema ha generado un error inesperado con su Última petición, intente ponerse en contacto con soporte
            técnico, a la brevedad posible.
        </div>

        <div>
            Cómuniquese con nosotros:<br>
            Email: {{getenv('SUPPORT_EMAIL')}} <br>
            Telefono (ext): {{getenv('SUPPORT_PHONE')}}

        </div>

        <p>&nbsp;</p>
        <div>
            <button id="back" onclick="history.back()" type="button" class="btn btn-default">Upps, salir de aqui
            </button>
        </div>
    </div>

@stop
{{--cerrar conntenido--}}
