@extends('layout.login')
@section('title', 'Módulo no Autorizado')
@section('head')
    <meta name="twitter:site" content="@delimce"/>
@stop
{{-- abrir el contenido--}}
@section('content')

    <div style="text-align: center">
    <div style="font-size: 50px; padding: 23px">
        <i class="fa fa-lock"></i>
        Acceso no autorizado
    </div>
    <div>
        <button id="back" onclick="history.back()" type="button" class="btn btn-default">Upps, salir de aqui</button>
    </div>
    </div>

@stop
{{--cerrar conntenido--}}
