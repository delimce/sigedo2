<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" sizes="16x16 32x32" type="image/x-icon" href="{{url('assets/images/devicon.png')}}">

    <title>
        @section('title')
            Administración SIGEDO 2.0
        @show
    </title>

    <!-- Bootstrap Core CSS -->
    {!! Html::style('assets/libs/bootstrap/dist/css/bootstrap.min.css') !!}
            <!-- MetisMenu CSS -->
    {!! Html::style('assets/libs/metisMenu/dist/metisMenu.min.css') !!}
            <!-- Custom CSS -->
    {!! Html::style('assets/css/sb-admin-2.css') !!}
            <!-- Custom Fonts -->
    {!! Html::style('assets/libs/font-awesome/css/font-awesome.min.css') !!}

            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('head')
</head>


<body>

<div class="container">
    @yield('content')
</div>
<div class="en text-center navbar-fixed-bottom">
    Desarrolado por: develemento.com.ve &copy; {{date("Y")}} Todos los derechos reservados.
</div>
<!-- jQuery -->
{!! Html::script('assets/libs/jquery/dist/jquery.min.js') !!}
        <!-- Bootstrap Core JavaScript -->
{!! Html::script('assets/libs/bootstrap/dist/js/bootstrap.min.js') !!}
        <!-- Metis Menu Plugin JavaScript -->
{!! Html::script('assets/libs/metisMenu/dist/metisMenu.min.js') !!}
        <!-- Custom Theme JavaScript -->
{!! Html::script('assets/js/admin-setup.js') !!}

@yield('foot')
</body>
</html>

