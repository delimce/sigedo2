<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" sizes="16x16 32x32" type="image/x-icon" href="{{url('assets/images/devicon.png')}}">

    <title>SIGEDO 2.0  @yield('title')</title>

    <!-- Bootstrap Core CSS -->
    {!! Html::style('assets/libs/bootstrap/dist/css/bootstrap.min.css') !!}
    {!! Html::style('assets/libs/bootstrap-dialog/css/bootstrap-dialog.css') !!}
            <!-- MetisMenu CSS -->
    {!! Html::style('assets/libs/metisMenu/dist/metisMenu.min.css') !!}
            <!-- Custom CSS -->
    {!! Html::style('assets/css/sb-admin-2.css') !!}
            <!-- Custom Fonts -->
    {!! Html::style('assets/libs/font-awesome/css/font-awesome.min.css') !!}

            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('head')
</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <div class="image-main">   {!! HTML::image('assets/images/contuy-logo.gif') !!}</div>

            <div class="navbar-brand proy-text">
                <span style="text-transform: uppercase" id="projectName">{{Session::get('OBRANOMBRE')}} </span>
            </div>
        </div>
        <!-- /.navbar-header -->
        <ul class="nav navbar-top-links navbar-right">
            <!-- Menu -->
            @include('admin.partials.options')
                    <!-- ./ Menu -->
        </ul>

        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">

            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <!-- Menu -->
                    @include('admin.partials.menu')
                            <!-- ./ Menu -->

                </ul>
            </div>

            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <!-- Page Content -->
    <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header"> @section('title') @show</h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-14">

                @yield('content')

            </div>
            <!-- /.col-lg-12 -->
        </div>


        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
{!! Html::script('assets/libs/jquery/dist/jquery.min.js') !!}
        <!-- Bootstrap Core JavaScript -->
{!! Html::script('assets/libs/bootstrap/dist/js/bootstrap.min.js') !!}
{!! Html::script('assets/libs/bootstrap-dialog/js/bootstrap-dialog.min.js') !!}
{!! Html::script('assets/libs/bootstrap-validator/validator.min.js') !!}
        <!-- Metis Menu Plugin JavaScript -->
{!! Html::script('assets/libs/metisMenu/dist/metisMenu.min.js') !!}
        <!-- Custom Theme JavaScript -->
{!! Html::script('assets/js/admin-setup.js') !!}

@yield('foot')
</body>
</html>